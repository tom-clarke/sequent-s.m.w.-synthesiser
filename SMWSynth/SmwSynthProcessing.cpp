#include "SmwSynth.h"
#include <math.h>

//------------------------------------------------------------------------------------
void SmwSynth::initProcessing ()
{
	sr = getSampleRate();	// get the sample rate

	// -- init oscillator --
	OscMan.reset		();			// initialise variables
	OscMan.setSampleRate(sr);		// update sample rate
	OscMan.setWavebank	(wavebank);	// set the wavebank
	updateEnv();				    // set the envelope params

	// -- init filter --
	BiQuad.setFilter(FTYPE_OFF, // type
		   parameters[kFilCutParam], // cut-off freq
		   parameters[kFilResParam], // quality
		   sr);						 // sample rate

	// -- setup MIDI events --
	num_events = 0; // init the number of events
	MIDIevents = 0; // init the event queue pointer
	MIDIevents = new VstMidiEvent[kMaxEvents]; // create the events
	for (int i = 0; i < kMaxEvents; i++)
	{
		MIDIevents[i].midiData[0] = 0; // init status byte
		MIDIevents[i].midiData[1] = 0; // init note byte
		MIDIevents[i].midiData[2] = 0; // init velocity byte
		MIDIevents[i].deltaFrames = -100; // init delta frames
		// delta frames shouldn't be negative so a value of
		// -100 indicates an empty event
	}
}

//------------------------------------------------------------------------------------
void SmwSynth::setSampleRate (float sampleRate)
{
	AudioEffectX::setSampleRate (sampleRate);	// set the VST sample rate

	sr = sampleRate;		  // store the sample rate
	OscMan.setSampleRate(sr); // update the oscillator
}

//------------------------------------------------------------------------------------
void SmwSynth::resume ()
{
	LogFile.post("resume called...\n");

	OscMan.reset(); // re-intialise the oscillator's internals

	AudioEffectX::resume (); // re-init VST internals
}

//------------------------------------------------------------------------------------
void SmwSynth::processReplacing (float** inputs, float** outputs, VstInt32 sampleFrames)
{
	int    i;
	float* out1 = outputs[0];	// create a pointer to
	float* out2 = outputs[1];	// each output channel

	float temp_out = 0.f; // to store the output before writing samples
	float temp_fil = 0.f;

	for (i = 0; i < sampleFrames; i++) // iterate through the audio block
	{
		// if there are any events in the queue
		// check the current position in the block against the event's 
		// delta time. if a match is found, the event is handled and cleared
		if (num_events > 0) handleMIDI(i);

		// get the output, filter, then scale by master volume level
		temp_out = OscMan.getSample();
		temp_out = BiQuad.getSample(temp_out) * parameters[kVolumeParam];
		*out1 = *out2 = temp_out; // both channels the same (mono)

		out1++; // advance the
		out2++; // pointers
	}

	// if there are still events in the queue
	// the delta time must refer to the next block of samples
	if (num_events > 0)
	{
		for (i = 0; i < kMaxEvents; i++) // check the whole queue
		{
			if (MIDIevents[i].deltaFrames != -100) // if the event isn't empty
				MIDIevents[i].deltaFrames -= sampleFrames; // update delta frames
		}
	}
}

	
// Event handling -----------------------------------------------------
VstInt32 SmwSynth::processEvents (VstEvents* ev)
{
	// check the number of events in the queue, and iterate through them
	for (int i = 0; i < ev->numEvents; i++)
	{
		// if the event is a midi event
		if ((ev->events[i])->type == kVstMidiType)
		{
			// point to the event temporarily
			VstMidiEvent* MIDIevent = (VstMidiEvent*)ev->events[i];
			int index = -1;

			// look for an empty event in the queue
			for (int j = 0; j < kMaxEvents; j++)
			{
				if (MIDIevents[j].deltaFrames == -100)
				{
					index = j; // store the index
					break;
				}
			}
			// then copy the data across
			if (index >= 0 && num_events < kMaxEvents) 
			{
				MIDIevents[index].midiData[0] = MIDIevent->midiData[0]; // store status
				MIDIevents[index].midiData[1] = MIDIevent->midiData[1]; // store note
				MIDIevents[index].midiData[2] = MIDIevent->midiData[2]; // store velocity
				MIDIevents[index].deltaFrames = MIDIevent->deltaFrames; // store delta
				num_events++; // update the event counter
			}
		}
	}
	sendVstEventsToHost(ev); // forward the events to chained plugins

	return 1;	// indicate that we were sucessful
}

//--------------------------------------------------
void SmwSynth::handleMIDI(int delta_pos)
{
	int   status = 0, note = 0, velocity = 0;
	char* midiData = 0; 

	for (int i = 0; i < kMaxEvents; i++)
	{
		// check the event is scheduled before our
		// delta_pos and is not an empty event.
		if (MIDIevents[i].deltaFrames <= delta_pos
			&& MIDIevents[i].deltaFrames != -100)
		{

			midiData = MIDIevents[i].midiData; // point to the midi data
			// get the status of the midi event
			// - first byte is the status byte, the & operand makes sure all MIDI channels
			//	 apart from channel 1 are ignored, along with status values above f0 and
			//   MIDI values above 127 (not needed/invalid)
			//   0x = tell the compiler this is a hex number
			//   f0 = our maximum status value (= system exclusive / data dump)
			//   7f = 127
			status   = midiData[0] & 0xf0;	// store status
			note     = midiData[1] & 0x7f;	// store note value
			velocity = midiData[2] & 0x7f;	// store velocity value

			switch (status)
			{
			case 0x90: // note on (channel 1)
				{
					// if the velocity is 0 this indicates a note off message
					if (velocity == 0) OscMan.noteOff (note);	// turn the note off

					// otherwise turn the note on
					else OscMan.noteOn (note, velocity);
				} 
				break;
			case 0x80: // note off (channel 1)
				{
					OscMan.noteOff(note); // turn the note off
				}
				break;
			case 0xb0: // control / mode change (channel 1)
				{
					// check the second byte of the MIDI message
					// 0x7b = all notes off
					// 0x7e = mono mode on (+ poly off, + all notes off)
					if (midiData[1] == 0x7b || midiData[1] == 0x7e)
						OscMan.noteOff(-1); // turn off all oscillators completely
				}
				break;
			}

			// make the event "empty" again
			MIDIevents[i].deltaFrames = -100; // reset the delta frames
			num_events--; // there is one less event in the queue
		}
	}
}