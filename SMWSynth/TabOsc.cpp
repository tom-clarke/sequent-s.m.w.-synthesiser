#include "TabOsc.h"

//---------------------------------------------------------------------------
CTabOsc::CTabOsc(void)
{
	tabsize = kWavetableSize; // defaults
	srate	= 44100.f;

	amplitude = 0.5f;	// -6 dB
	frequency = 440.f;	// concert A

	inc = 0.f;			// increment
	last_output = 0.f;  // output check
	fReadIndex = 0.f;	// floating-point index (phase)
	iBankIndex = 0;		// integer read index

	iReadIndex	   = 0;	// truncated read index
	iReadIndexNext = 0;	// next read index
	y1 = y2 = y3 = y4 = 0.f;	// interp function params
	interp = 0.f;	// interpolated value
	frac   = 0.f;	// remainder from truncation
	outAmp = 0.5f;	// final amplitude value

	// FOR MIDI CONTROL:
	// make frequency (Hz) table
	double  k = 1.059463094359;	// 12th root of 2
	double  a = 6.875; // = A
			a *= k;	  // = Bb
			a *= k;	  // = B
			a *= k;	  // = C, frequency of midi note 0
	for (int i = 0; i < kNumMidiNotes; i++)	// 128 midi notes
	{
		// store midi note
		freqtab[i] = (float)a;
		// calculate next frequency
		a *= k;
	}

	mask = kWavetableSize -1;

	wavebank.clear();
	signal.clear();

	Env = new CEnvelope ();
	Env->setGate(false);
	Env->reset();

	isOn = false;
}

//---------------------------------------------------------------------------
CTabOsc::~CTabOsc(void)
{
	delete Env; // free the envelope from memory
}
//---------------------------------------------------------------------------
void CTabOsc::setIncrement()
{
	inc = (frequency * (tabsize / srate));
}

void CTabOsc::setWaveSignal(std::vector<float> sig)
{
	signal = sig;
	mask = signal.size() - 1; // update mask
}

//---------------------------------------------------------------------------
void CTabOsc::setADSR(float a, float d, float s, float r)
{
	Env->setAttackTime  (a);
	Env->setSustainLevel(s); // need to call this before decay and release
	Env->setDecayTime   (d);
	Env->setReleaseTime (r);

	Env->setSampleRate(srate);
}

//---------------------------------------------------------------------------
void CTabOsc::reset()
{
	fReadIndex   = 0.f; 
	iBankIndex	   = 0; 
	iReadIndex	   = 0;	// truncated read index
	iReadIndexNext = 0;	// next read index
	y1 = y2 = y3 = y4 = 0.f; // interp params
	interp = 0.f;	// interpolated value
	frac   = 0.f;	// remainder from truncation

	amplitude = 0.5f;
	outAmp = 0.5f;	// final amplitude value

	isOn = false;

	Env->reset();
}

//---------------------------------------------------------------------------
float CTabOsc::getSample()
{	
	float output = 0.f;	// start with a zero value

	iReadIndex	 = (VstInt32) fReadIndex;	// get the truncated table index

	// get values from wavebank at index postions
	// bank index determines waveform vector
	y1 = signal.at(iReadIndex & mask);
	y2 = signal.at(iReadIndex + 1 & mask);
	y3 = signal.at(iReadIndex + 2 & mask);
	y4 = signal.at(iReadIndex + 3 & mask);

	frac = fReadIndex - (float)iReadIndex;	// get the fractal part of the phase index
	interp = lagrangeInterp(y1, y2, y3, y4, frac); // interpolate to determine output value

	outAmp = amplitude * Env->process(); // get final amplitude
	if (outAmp <= 0.f) isOn = false;	
	output += interp * outAmp;	// multiply output value by interpolated value
			
	fReadIndex += inc;	// increment the read / phase index
	
	last_output = output;
	return output;	// return the final output sample
}

//---------------------------------------------------------------------------
void CTabOsc::noteOn (int note, int velocity)
{ 
	reset(); // get variables ready for new note
	setFrequency(freqtab[note]); // set the frequency from the table
	
	float vel = (float)velocity / 128.f; // convert midi velocity to between 0 and 1
	if (vel > 1.f) vel = 1.f;			 // keep under max amplitude
	setAmplitude(vel);  // set volume using converted value

	Env->setGate(true);	// trigger the envelope's gate

	isOn = true; // indicate that the oscillator is now playing
}

//---------------------------------------------------------------------------
void CTabOsc::noteOff ()
{
	if (isOn)
	{
		Env->setGate(false);	// trigger the envelope's release section
	}
}

//---------------------------------------------------------------------------
void CTabOsc::turnOff ()
{
	isOn = false;	// inidicate that the oscillator is not playing anymore
	reset();		// reset our params
	Env->reset();	// reset the envelope ready for next time
}

// this checks numbers to see if they are valid
bool CTabOsc::isNumber(double x) 
{
    // This looks like it should always be true, 
    // but it's false if x is a NaN.
    return (x == x); 
}