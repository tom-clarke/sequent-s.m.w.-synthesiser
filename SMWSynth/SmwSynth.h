//-------------------------------------------------------------------------------------------------------
//	SmwSynth - Sequential Multiple Wavetable Synthesiser with Wavetable Editor
//  VSTSDK Version 2.4
//-------------------------------------------------------------------------------------------------------
/*
VST 2.4 Build Settings

General
	Character Set: Not Set
	Common Language Runtime Support: No Common Language Runtime Support
	
C/C++
	General:
		Additional Include Directories:
			"C:\vstsdk2.4"
			"C:\vstsdk2.4\public.sdk\source\vst2.x"

	Preprocessor:
		Preprocessor Definitions:
			WINDOWS;_WINDOWS;WIN32;_USRDLL;_USE_MATH_DEFINES			
			If using PNG graphics for VSTGUI, add
			USE_LIBPNG=1
			To avoid deprecated warnings, define
			_CRT_SECURE_NO_DEPRECATE

	Code Generation:
		Runtime Library: Multi-threaded. Multi-threaded debug for debug builds. 

	Precompiled Headers:
		Precompiled Header: Not Using Precompiled Headers.

Linker
	General:
		Additional Library Directories: Add other library directories that the project depends on.

	Input:
		Additional Dependencies (Debug):
			shell32.lib;msvcrtd.lib;ole32.lib;gdi32.lib;User32.lib;advapi32.lib;
			zlib.lib (only if you are building a custom GUI using PNGs)
			libpng.lib (only if you are building a custom GUI using PNGs)
			
		Ignore Specific Default Library (Debug):
			libcmt.lib;libcmtd.lib;msvcrt.lib;

		Additional Dependencies (Release):
			libcmt.lib;uuid.lib;shell32.lib;ole32.lib;gdi32.lib;User32.lib;advapi32.lib;
			zlib.lib (only if you are building a custom GUI using PNGs)
			libpng.lib (only if you are building a custom GUI using PNGs)

		Ignore Specific Default Library (Release):
			msvcrt.lib;libc.lib;msvcrtd.lib;libcd.lib;libcmtd.lib;

		Module Definition File: ProjectName.def
*/
//-------------------------------------------------------------------------------------------------------
#pragma once

#include "gmnames.h"
#include <vector>

#include "SmwSynthEditor.h"
#include "Envelope.h"
#include "OscManager.h"
#include "BQFilter.h"

class SmwSynth;
class SmwSynthEditor;

//---------------------------------------------------------------------------
class SmwSynthProgram
{
friend class SmwSynth;
public:
	SmwSynthProgram ();
	~SmwSynthProgram () {}

private:
	float parameters[kNumParameters];

	char name[kVstMaxProgNameLen];
};

//---------------------------------------------------------------------------
class SmwSynth : public AudioEffectX
{
public:

	SmwSynth(audioMasterCallback audioMaster);
	~SmwSynth();

	virtual void setSampleRate (float sampleRate);	// set sample rate and update oscs, filter

	virtual void processReplacing (float** inputs, float** outputs, VstInt32 sampleFrames);
	virtual VstInt32 processEvents (VstEvents* ev);
	virtual void resume ();	// clear buffers / get ready for play

	virtual void setParameter (VstInt32 index, float value);
	virtual float getParameter (VstInt32 index);

	virtual void setProgram (VstInt32 program);
	virtual void setProgramName (char* name);
	virtual void getProgramName (char* name);
	virtual bool getProgramNameIndexed (VstInt32 category, VstInt32 index, char* text);
	
	virtual bool getOutputProperties (VstInt32 index, VstPinProperties* properties);
		
	virtual bool getEffectName (char* name);
	virtual bool getVendorString (char* text);
	virtual bool getProductString (char* text);
	virtual VstInt32 getVendorVersion ();
	virtual VstInt32 canDo (char* text);

	virtual VstInt32 getNumMidiInputChannels ();
	virtual VstInt32 getNumMidiOutputChannels ();

	virtual VstInt32 getMidiProgramName (VstInt32 channel, MidiProgramName* midiProgramName);
	virtual VstInt32 getCurrentMidiProgram (VstInt32 channel, MidiProgramName* currentProgram);
	virtual VstInt32 getMidiProgramCategory (VstInt32 channel, MidiProgramCategory* category);
	virtual bool hasMidiProgramsChanged (VstInt32 channel);
	virtual bool getMidiKeyName (VstInt32 channel, MidiKeyName* keyName);

	void updateEnv(); // updates all the envelope params
	
private:
	float parameters[kNumParameters];	// parameters of the plugin including displays
	SmwSynthEditor* SmwEditor;			// a pointer to the GUI editor

	std::vector<std::vector<float>> wavebank;	// 2d vector for the wavebank

	COscManager   OscMan;	// an oscillator manager for polyphony
	CBQFilter     BiQuad;   // a bi-quadratic filter instance

	SmwSynthProgram* programs;		// pointer to a program / preset (array)
	VstInt32 channelPrograms[16];	// MIDI program variables
	
	float sr; // the current sample rate

	// MIDI data variables
	VstInt32 currentNote;
	VstInt32 currentVelocity;
	VstInt32 currentDelta;
	VstMidiEvent* MIDIevents;
	int num_events; // a counter for the amount of active events

	// other functions
	void initProcessing ();	// setup synthesis variables etc.
	void handleMIDI (int delta_pos); // executes MIDI events by deltaframe

	// for MIDI GM names...
	void fillProgram (VstInt32 channel, VstInt32 prg, MidiProgramName* mpn);
};
