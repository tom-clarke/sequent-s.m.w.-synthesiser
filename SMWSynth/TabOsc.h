#pragma once

#include "audioeffectx.h"
#include "Parameters.h"
#include <vector>

#include "Envelope.h"
#include "DebugLog.h"
#include "Interp.h"

static float freqtab[kNumMidiNotes];	// a freq for each MIDI note


//-------------------------------------------------------------------
class CTabOsc
{
public:
	CTabOsc(void);
	~CTabOsc(void);

	void setAmplitude	(float amp)	{ amplitude = amp; }
	void setSampleRate	(float sr)	{ srate = sr; }
	void setWaveSignal	(std::vector<float> sig);
	void setADSR		(float a, float d, float s, float r);

	void reset();

	void noteOn(int note, int velocity); 
	void noteOff();
	void turnOff();
	bool isPlaying() { return isOn; }

	float getSample ();
	float getOutput () { return last_output; }

private:

	std::vector<float> signal;
	std::vector<std::vector<float>> wavebank;

	void  setIncrement ();
	void  setFrequency (float hz) { frequency = hz; setIncrement (); }

	bool  isNumber       (double x);

	CEnvelope* Env;

	bool isOn;

	float last_output;

	// calculation variables
	long  tabsize;
	float srate;

	float amplitude;
	float frequency;

	float inc;
	float fReadIndex;
	long  iBankIndex;

	VstInt32  iReadIndex;		// truncated read index
	VstInt32  iReadIndexNext;	// next read index
	float y1, y2, y3, y4;	// supplied to interp function
	float interp;	// interpolated value
	float frac;		// remainder from truncation
	float outAmp;	// final amplitude value

	VstInt32 mask;
};

