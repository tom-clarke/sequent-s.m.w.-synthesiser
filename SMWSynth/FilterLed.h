#pragma once

#include "vstgui.h"

class CFilterLed : public COnOffButton
{
public:
	CFilterLed(const CRect& size, CControlListener* listener, long tag, CBitmap* background, long style = kPreListenerUpdate);
	~CFilterLed();

	CMouseEventResult onMouseDown (CPoint& where, const long& buttons);

private:
	long style;
};

