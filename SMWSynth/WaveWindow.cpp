#include "WaveWindow.h"

//---------------------------------------------------------------------------
CWaveWindow::CWaveWindow(const CRect &size, CControlListener *listener, long tag, 
		CBitmap *pBackground)	// bitmap of the background
		: CControl(size, listener, tag)
{
	winpoints.clear();
	// set up waveform points?
	for (int i = 0; i < kWaveWinTableLength; i++)
	{
		winpoints.push_back(0.0);
	}

	setBackground (pBackground);

	firstPoint(-1,-1);
	lastPoint(-1,-1);
	oldButton = -1;

	mouseDown(-1,-1);

	setTransparency (false);

	setWantsFocus(true);
}

//---------------------------------------------------------------------------
CWaveWindow::~CWaveWindow()
{

}

//---------------------------------------------------------------------------
void CWaveWindow::draw(CDrawContext *pContext)
{
	// if theres a background image
	if (pBackground)
	{
		if (bTransparencyEnabled)
			pBackground->drawTransparent (pContext, size, CPoint(0,0));
		else
			pBackground->draw (pContext, size, CPoint(0,0));
	}

	drawWave (pContext);

	setDirty (false);
}

//---------------------------------------------------------------------------
void CWaveWindow::drawWave(CDrawContext *pContext)
{
	// draw middle line
	CColor colourBlack (kBlackCColor); 
	CColor colourGreen (kGreenCColor);

	pContext->setLineWidth(1);
	pContext->setFrameColor(colourBlack);
	pContext->moveTo(CPoint(kWaveWinX + 2, kWaveWinY + 75));
	pContext->lineTo(CPoint(kWaveWinX - 2 + kWaveWinTableLength, kWaveWinY + 75));

	pContext->setLineWidth(2);
	pContext->setFrameColor(colourGreen);
	
	// draw the waveform points here...
	int Xstart, Ystart, Xcoord, Ycoord;
	Xstart = kWaveWinX + 2;
	Ystart = kWaveWinY + 75;
	pContext->moveTo(CPoint(Xstart,Ystart));
	
	for (int i = 0; i < kWaveWinTableLength - 1; i++)
	{
		// update the coordinates
		Xcoord = Xstart + i;
		Ycoord = Ystart + (int)winpoints.at(i);

		// draw a line to the next point
		pContext->lineTo(CPoint(Xcoord,Ycoord));
	}
}

//---------------------------------------------------------------------------
CMouseEventResult CWaveWindow::onMouseDown(CPoint &where, const long &buttons)
{
	// check if any mouse buttons are pressed
	if (!(buttons & kLButton))
		return kMouseEventNotHandled;

	beginEdit ();	// start editing

	firstPoint = where; // setup initial location
	int fpX = (int)firstPoint.x - 2 - kWaveWinX; // get coords
	int fpY = (int)firstPoint.y - 75 - kWaveWinY;

	// Make sure the values are in range
	if (fpX < 0) fpX = 0;
	if (fpX >= kWaveWinTableLength) fpX = kWaveWinTableLength - 1;
	if (fpY < -75) fpX = -75;
	if (fpY > 75) fpX = 75;

	winpoints.at(fpX) = (float)(fpY);
	lastPoint = firstPoint; // initialise previous point
	mouseDown = firstPoint;

	oldButton = buttons; // store last button

	return onMouseMoved(where, buttons);
}

//---------------------------------------------------------------------------
CMouseEventResult CWaveWindow::onMouseMoved (CPoint &where, const long& buttons)
{
	if (buttons & kLButton)
	{
		if (where != lastPoint)
		{
			// Need to update these for every mouse moved event
			// for base classes to operate correctly
			firstPoint = lastPoint;
			lastPoint  = where;

			// Lagrange Interpolation as wave drawing mode only
			// if default value is required (ctrl + click)
			if (checkDefaultValue (buttons))
			{
				// Find out which direction the mouse drag is going
				// (+)diff = left > right
				// (-)diff = left < right
				int diff  = where.x - mouseDown.x;

				// Set the start and end interpolation points to
				// the first and one-past-the-last point in the 
				// table, initializing y values to 0.
				CPoint begin, end;
				begin.x = 0.0;
				begin.y = 0.0;
				end.x = 256.0;
				end.y = 0.0;

				// Convert the actual mouse click locations to
				// be relative to the waveform editor.
				CPoint down, up;
				down.x = mouseDown.x - 2 - kWaveWinX;
				down.y = mouseDown.y - 75 - kWaveWinY;
				up.x = where.x - 2 - kWaveWinX;
				up.y = where.y - 75 - kWaveWinY;

				if (down.x < 0) down.x = 0;
				if (down.x > 255) down.x = 255;
				if (down.y > 70) down.y = 70;
				if (down.y < -70) down.y = -70;
				if (up.x < 0) up.x = 0;
				if (up.x > 255) up.x = 255;
				if (up.y > 70) up.y = 70;
				if (up.y < -70) up.y = -70;

				// Comment
				if (diff >= 0) {
					for (int i = 0; i < kWaveWinTableLength; ++i)
					{
						winpoints[i] = lagrangeInterp(begin,
													down,
													up,
													end,
													i);
						if (winpoints[i] > 70) winpoints[i] = 70;
						if (winpoints[i] < -70) winpoints[i] = -70;
					}
				}
				else if (diff < 0) {
					for (int i = 0; i < kWaveWinTableLength; ++i)
					{
						winpoints[i] = lagrangeInterp(begin,
													up,
													down,
													end,
													i);
						if (winpoints[i] > 70) winpoints[i] = 70;
						if (winpoints[i] < -70) winpoints[i] = -70;
					}
				}
			} 
			// Otherwise draw the waveform free hand
			else { 

				int Xlocal = lastPoint.x-2-kWaveWinX;
				int Ylocal = lastPoint.y - 75 - kWaveWinY;

				// Make sure the values are in range
				if (Xlocal < 0) Xlocal = 0;
				if (Xlocal > 255) Xlocal = 255;
				if (Ylocal >  70) Ylocal = 70;
				if (Ylocal < -70) Ylocal = -70;

				int diff  = firstPoint.x - where.x;

				if (Xlocal + diff > 0 && Xlocal + diff < kWaveWinTableLength
					&& Xlocal > 0 && Xlocal < kWaveWinTableLength)
				{	
					if (diff > 0)
					{
						winpoints.at(Xlocal) = (float)Ylocal;
						
						for (int i = 0; i < diff; i++)
						{
							winpoints.at(Xlocal + i) = linearInterp(winpoints.at(Xlocal),winpoints.at(Xlocal+diff),i*(1.f/diff));
						}
					}
					else if (diff < 0)
					{
						winpoints.at(Xlocal) = (float)Ylocal;

						for (int i = 0; i > diff; i--)
						{
							winpoints.at(Xlocal + i) = linearInterp(winpoints.at(Xlocal),winpoints.at(Xlocal+diff),i*(1.f/diff));
						}	
					}
					else
					{
						winpoints.at(Xlocal) = (float)Ylocal;
					}
				}
			}

			invalid ();
			return onMouseMoved (where, buttons);
			
		}
	}
	return kMouseEventHandled;
}

//---------------------------------------------------------------------------
CMouseEventResult CWaveWindow::onMouseUp (CPoint &where, const long& buttons)
{
	endEdit();
	applyFade();
	invalid();
	return kMouseEventHandled;
}

//---------------------------------------------------------------------------
void CWaveWindow::applyFade()
{
	int i, j = 0;
	int outstart = 0;
	float incr = 0.f;
	
	incr = 1.f / kFadeLength;
	for (i = 0; i < kFadeLength; i++)
	{
		winpoints.at(i) = linearInterp(0.f, winpoints.at(kFadeLength), i*incr);
	}
	
	outstart = kWaveWinTableLength - kFadeLength;
	for (i = outstart; i < kWaveWinTableLength; i++)
	{
		winpoints.at(i) = linearInterp(winpoints.at(outstart), 0.f, j*incr);
		j++;
	}
}

//---------------------------------------------------------------------------
std::vector<float> CWaveWindow::getWindowPoints()
{
	std::vector<float> temp; // create a temporary vector
	temp.clear(); // make sure its empty

	float current = 0.f, next = 0.f;
	float interp  = 0.f;

	//char sample[12]; // for message data

	// iterate through the display points
	for (int i = 0; i < kWaveWinTableLength; i++)
	{
		current = winpoints.at(i);	// get the current point
		temp.push_back(current);	// store that first

		if (i < kWaveWinTableLength - 1) // if the next point is available
		{
			next = winpoints.at(i+1); // get the next point
		}
		else if (i == kWaveWinTableLength - 1) // or if we have reached the end
		{
			next = winpoints.at(0);  // get the first point
		}
		else // if any other value of i is present
		{
			LogFile.post("getWindowPoints: target out of range!\n");
			break; // exit the for loop
		}

		interp = linearInterp(current, next, 0.5f); // find halfway point
		temp.push_back(interp); // store the new point

		// VALUE CHECK //
		//sprintf(sample,"%f\n",interp); // convert point to string
		//LogFile.post(sample); // post in debug log file
	}

	// check if we have the requested amount of points in the vector
	if (temp.size() != kWavetableSize) LogFile.post("size doesnt match!\n");
	
	return temp;
}

void CWaveWindow::setWindowPoints(std::vector<float> newpoints)
{
	std::vector<float> temp; // create a temporary vector
	temp.clear(); // make sure its empty

	float value = 0.f; // the value to store
	//char sample[12]; // for sample output to debug log

	if (newpoints.size() != kWavetableSize)	// if we have an invalid size
	{
		LogFile.post("setWindowPoints: incoming vector size invalid!\n");
		return; // exit the function
	}

	// iterate through display vector size
	for (int i = 0; i < kWaveWinTableLength; i++)
	{
		value = newpoints.at(i * 2);
		temp.push_back(value);

		// VALUE CHECK //
		//sprintf(sample,"%f\n",value); // convert point to string
		//LogFile.post(sample); // post in debug log file
	}

	// if the size of the temp vector is not correct
	if (temp.size() != kWaveWinTableLength) 
	{
		LogFile.post("setWindowPoints: size doesn't match!\n");
		return; // exit the function
	}

	winpoints = temp; // update the display points
}