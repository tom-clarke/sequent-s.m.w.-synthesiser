#pragma once

#include <fstream>
#include <ctime>

class CDebugLog
{
public:
	CDebugLog()  { curr_time = 0.0; logfile.open("smwsynth.log");
									wlogfile.open("smwsynthw.log"); }
	~CDebugLog() { logfile.close(); wlogfile.close(); }

	bool init();
	void post(char* msg);
	void post(std::wstring msg);

private:
	void timestamp();

	std::ofstream logfile;
	std::wfstream wlogfile;
	double        curr_time;
};

static CDebugLog LogFile; // create an instance that will stay in one memory location
