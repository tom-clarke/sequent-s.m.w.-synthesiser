#include "SmwSynth.h"
#include "aeffguieditor.h"
#include <stdio.h>

//---------------------------------------------------------------------------
AudioEffect* createEffectInstance (audioMasterCallback audioMaster)
{
	return new SmwSynth (audioMaster);
}

//---------------------------------------------------------------------------
SmwSynthProgram::SmwSynthProgram()
{
	for (int i = 0; i < kNumParameters; i++)
	{
		parameters[i] = 0.f;
	}

	// set program param defaults
	parameters[kVolumeParam] = 0.7f;
	parameters[kEnvAttParam] = 0.01f;
	parameters[kEnvDecParam] = 0.2f;
	parameters[kEnvSusParam] = 0.5f;
	parameters[kEnvRelParam] = 0.3f;

	parameters[kFilCutParam] = 1.f;
	parameters[kFilResParam] = 0.3f;
	parameters[kFilOffParam] = 1.f;

	// set default program name
	vst_strncpy(name, "Default", kVstMaxProgNameLen);
}

//---------------------------------------------------------------------------
SmwSynth::SmwSynth(audioMasterCallback audioMaster)
	: AudioEffectX (audioMaster, kNumPrograms, kNumParameters)
{
	// set a unique ID for the plugin
	setUniqueID('5mwS');

	// set the number of inputs and outputs
	setNumInputs(0);
	setNumOutputs(kNumOutputs);

	canProcessReplacing();	// supports replacing output
	isSynth();				// its a synthesiser

	// setup MIDI programs
	for (VstInt32 i = 0; i < 16; i++)
		channelPrograms[i] = i;
	// assign memory to the programs
	programs = new SmwSynthProgram[kNumPrograms];

	// check for presets, if they exist select the first one
	if (programs) setProgram (0);
	else LogFile.post("Error: no programs!\n");

	initProcessing(); // setup processing variables etc.

	wavebank.clear(); // make sure the wavebank is empty

	extern AEffGUIEditor* createEditor (AudioEffectX*);
	setEditor (SmwEditor = (SmwSynthEditor*)createEditor(this));

	// debug file opening message
	LogFile.post("SmwSynth Initiated OK.\n");

	suspend();
}

//---------------------------------------------------------------------------
SmwSynth::~SmwSynth(void)
{
	if (MIDIevents) delete [] MIDIevents; // clear memory for MIDI event queue
}

//------------------------------------------------------------------------------------
void SmwSynth::setParameter (VstInt32 index, float value)
{
	if (index < kNumParameters) // if we have a valid parameter index
	{	
		switch (index)
		{
			// -- wavebank -------------------------------------------------
			// if any of the wave editor controls have been pressed...
		case kBtnNewParam:
		case kBtnAddParam:
		case kBtnDelParam:
		case kBtnLoadBankParam:
		case kBtnLoadWaveParam:
			OscMan.reset(); // stop all sound
				
			// check that there is a valid pointer to the editor and update wavebank
			if (SmwEditor) OscMan.setWavebank(wavebank = SmwEditor->getWaveBank());
				
			// update the parameter relating to bank size
			parameters[kWaveWinParam] = (float)wavebank.size();
			
			// update the display
			if (editor) ((AEffGUIEditor*)editor)->setParameter (index, value);
			break;

			// -- current wave ---------------------------------------
			// update display
		case kUpSelectParam:   if (editor) ((AEffGUIEditor*)editor)->setParameter (index, value); break;
		case kDownSelectParam: if (editor) ((AEffGUIEditor*)editor)->setParameter (index, value); break;

			// -- envelope -------------------------------------------
			// if any envelope controls have been pressed...
		case kEnvAttParam:
		case kEnvDecParam:
		case kEnvSusParam:
		case kEnvRelParam: parameters[index] = value;						    // update the parameter
			if (editor) ((AEffGUIEditor*)editor)->setParameter (index, value);  // update editor
			updateEnv();														// update envelopes
			break;

			// -- update the filter ----------------------------------
		case kFilCutParam: parameters[index] = value;							// update the parameter
			if (editor) ((AEffGUIEditor*)editor)->setParameter (index, value);	// update editor
			BiQuad.setCenterFreq(parameters[kFilCutParam]);						// update filter cut-off
			parameters[kFilCutDisplay] = value;
			if (editor) ((AEffGUIEditor*)editor)->setParameter (kFilCutDisplay, value);	// update editor
			break;
		case kFilResParam: parameters[index] = value;							// update the parameter 
			if (editor) ((AEffGUIEditor*)editor)->setParameter (index, value);	// update editor
			BiQuad.setQuality(parameters[kFilResParam]);					    // update filter quality
			parameters[kFilQDisplay] = value;
			if (editor) ((AEffGUIEditor*)editor)->setParameter (kFilQDisplay, value);	// update editor
			break;
		case kFilShape1Param: parameters[index] = value;						// update the parameter 
			if (editor) ((AEffGUIEditor*)editor)->setParameter (index, value);	// update editor
			BiQuad.setType(FTYPE_LOW_PASS);										// update filter type
			break;
		case kFilShape2Param: parameters[index] = value;						// update the parameter 
			if (editor) ((AEffGUIEditor*)editor)->setParameter (index, value);	// update editor
			BiQuad.setType(FTYPE_BAND_STOP);									// update filter type
			break;
		case kFilShape3Param: parameters[index] = value;						// update the parameter 
			if (editor) ((AEffGUIEditor*)editor)->setParameter (index, value);  // update editor
			BiQuad.setType(FTYPE_HIGH_PASS);									// update filter type
			break;
		case kFilOffParam: parameters[index] = value;							// update the parameter 
			if (editor) ((AEffGUIEditor*)editor)->setParameter (index, value);  // update editor
			BiQuad.setType(FTYPE_OFF);									// update filter type
			break;

			// -- all other parameters -----------------------------
		default: parameters[index] = value; // update the parameter and update the editor
			if (editor) ((AEffGUIEditor*)editor)->setParameter (index, value); break;
		}
	}
}

//------------------------------------------------------------------------------------
void SmwSynth::updateEnv() 
{ 
	// update all the envelopes parameters with values obtained from the editor
	OscMan.setADSR( parameters[kEnvAttParam],
					parameters[kEnvDecParam],
					parameters[kEnvSusParam],
					parameters[kEnvRelParam]); 
}

//------------------------------------------------------------------------------------
float SmwSynth::getParameter (VstInt32 index)
{
	// this is so that when the plugin loads, the generated sine wave is added to
	// the wavebank, otherwise there would be no sound output
	if (index == kBtnAddParam) setParameter(kBtnAddParam, 1.f);

	if (index < kNumParameters)
		return parameters[index];
	return 0.f;
}

//-----------------------------------------------------------------------------------------
bool SmwSynth::getOutputProperties (VstInt32 index, VstPinProperties* properties)
{
	// if current output pin is less than our max outputs
	if (index < kNumOutputs)
	{
		vst_strncpy (properties->label, "Vstx ", 63); // give it a name

		char temp[11] = {0};
		int2string (index + 1, temp, 10);	// get the pin number as a string
		vst_strncat (properties->label, temp, 63); // append it to the name

		properties->flags = kVstPinIsActive; // set the pin active

		if (index < 2)		// unless we have stereo output, then we
			properties->flags |= kVstPinIsStereo;	// make channel 1+2 stereo

		return true; // indicate success
	}
	return false;
}

// ** Program functions **
//------------------------------------------------------------------------
void SmwSynth::setProgram (VstInt32 program)
{
	// if the index is out of range, return - bad access
	if (program < 0 || program >= kNumPrograms)
		return;
	
	// create a pointer to the relevant program
	SmwSynthProgram* ap = &programs[program];
	curProgram = program; // set our current program index

	// set the parameter values stored in the program
	for (int i = 0; i < kNumParameters; i++)
	{
		parameters[i] = ap->parameters[i];
	}
}

//------------------------------------------------------------------------
void SmwSynth::setProgramName (char* name)
{
	// update the program's name variable
	vst_strncpy (programs[curProgram].name, name, kVstMaxProgNameLen);
}

//------------------------------------------------------------------------
void SmwSynth::getProgramName (char* name)
{
	// get the program's name variable
	vst_strncpy (name, programs[curProgram].name, kVstMaxProgNameLen);
}

//-----------------------------------------------------------------------------------------
bool SmwSynth::getProgramNameIndexed (VstInt32 category, VstInt32 index, char* text)
{
	// get the indexed program name, if it is within bounds
	if (index < kNumPrograms)
	{
		vst_strncpy (text, programs[index].name, kVstMaxProgNameLen);
		return true;
	}
	return false;
}

//-----------------------------------------------------------------------------------------
bool SmwSynth::getEffectName (char* name)
{
	vst_strncpy (name, "Sequent", kVstMaxEffectNameLen);
	return true;
}

//-----------------------------------------------------------------------------------------
bool SmwSynth::getVendorString (char* text)
{
	vst_strncpy (text, "Sine Junky Plugins", kVstMaxVendorStrLen);
	return true;
}

//-----------------------------------------------------------------------------------------
bool SmwSynth::getProductString (char* text)
{
	vst_strncpy (text, "Sequential Multi-Wavetable Synth", kVstMaxProductStrLen);
	return true;
}

//-----------------------------------------------------------------------------------------
VstInt32 SmwSynth::getVendorVersion ()
{ 
	return 1000; 
}

//-----------------------------------------------------------------------------------------
VstInt32 SmwSynth::canDo (char* text)
{
	if (!strcmp (text, "receiveVstEvents"))
		return 1;
	if (!strcmp (text, "receiveVstMidiEvent"))
		return 1;
	if (!strcmp (text, "midiProgramNames"))
		return 1;
	return -1;	// explicitly can't do; 0 => don't know
}

//-----------------------------------------------------------------------------------------
VstInt32 SmwSynth::getNumMidiInputChannels ()
{
	return 1; // we have input from one MIDI channel
}

//-----------------------------------------------------------------------------------------
VstInt32 SmwSynth::getNumMidiOutputChannels ()
{
	return 0; // no MIDI output back to Host app
}

// MIDI program names: Steinberg code
//------------------------------------------------------------------------
VstInt32 SmwSynth::getMidiProgramName (VstInt32 channel, MidiProgramName* mpn)
{
	VstInt32 prg = mpn->thisProgramIndex;
	if (prg < 0 || prg >= 128)
		return 0;
	fillProgram (channel, prg, mpn);
	if (channel == 9)
		return 1;
	return 128L;
}

//------------------------------------------------------------------------
VstInt32 SmwSynth::getCurrentMidiProgram (VstInt32 channel, MidiProgramName* mpn)
{
	if (channel < 0 || channel >= 16 || !mpn)
		return -1;
	VstInt32 prg = channelPrograms[channel];
	mpn->thisProgramIndex = prg;
	fillProgram (channel, prg, mpn);
	return prg;
}

//------------------------------------------------------------------------
void SmwSynth::fillProgram (VstInt32 channel, VstInt32 prg, MidiProgramName* mpn)
{
	mpn->midiBankMsb =
	mpn->midiBankLsb = -1;
	mpn->reserved = 0;
	mpn->flags = 0;

	if (channel == 9)	// drums
	{
		vst_strncpy (mpn->name, "Standard", 63);
		mpn->midiProgram = 0;
		mpn->parentCategoryIndex = 0;
	}
	else
	{
		vst_strncpy (mpn->name, GmNames[prg], 63);
		mpn->midiProgram = (char)prg;
		mpn->parentCategoryIndex = -1;	// for now

		for (VstInt32 i = 0; i < kNumGmCategories; i++)
		{
			if (prg >= GmCategoriesFirstIndices[i] && prg < GmCategoriesFirstIndices[i + 1])
			{
				mpn->parentCategoryIndex = i;
				break;
			}
		}
	}
}

//------------------------------------------------------------------------
VstInt32 SmwSynth::getMidiProgramCategory (VstInt32 channel, MidiProgramCategory* cat)
{
	cat->parentCategoryIndex = -1;	// -1:no parent category
	cat->flags = 0;					// reserved, none defined yet, zero.
	VstInt32 category = cat->thisCategoryIndex;
	if (channel == 9)
	{
		vst_strncpy (cat->name, "Drums", 63);
		return 1;
	}
	if (category >= 0 && category < kNumGmCategories)
		vst_strncpy (cat->name, GmCategories[category], 63);
	else
		cat->name[0] = 0;
	return kNumGmCategories;
}

//------------------------------------------------------------------------
bool SmwSynth::hasMidiProgramsChanged (VstInt32 channel)
{
	return false;	// updateDisplay ()
}

//------------------------------------------------------------------------
bool SmwSynth::getMidiKeyName (VstInt32 channel, MidiKeyName* key)
								// struct will be filled with information for 'thisProgramIndex' and 'thisKeyNumber'
								// if keyName is "" the standard name of the key will be displayed.
								// if false is returned, no MidiKeyNames defined for 'thisProgramIndex'.
{
	// key->thisProgramIndex;		// >= 0. fill struct for this program index.
	// key->thisKeyNumber;			// 0 - 127. fill struct for this key number.
	key->keyName[0] = 0;
	key->reserved = 0;				// zero
	key->flags = 0;					// reserved, none defined yet, zero.
	return false;
}