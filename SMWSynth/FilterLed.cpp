#include "FilterLed.h"


CFilterLed::CFilterLed(const CRect& size, CControlListener* listener, long tag, CBitmap* background, long style)
	: COnOffButton (size, listener, tag, background), style (style)
{

}


CFilterLed::~CFilterLed()
{

}

CMouseEventResult CFilterLed::onMouseDown (CPoint& where, const long& buttons)
{
	if (!(buttons & kLButton))
		return kMouseEventNotHandled;

	value = ((long)value) ? 0.f : 1.f;

	invalid ();

	if (listener && style == kPostListenerUpdate)
	{
		// begin of edit parameter
		beginEdit ();
	
		listener->valueChanged (this);
	
		// end of edit parameter
		endEdit ();
	}
	
	doIdleStuff ();
	
	if (listener && style == kPreListenerUpdate)
	{
		// begin of edit parameter
		beginEdit ();
	
		listener->valueChanged (this);
	
		// end of edit parameter
		endEdit ();
	}

	return kMouseDownEventHandledButDontNeedMovedOrUpEvents;
}