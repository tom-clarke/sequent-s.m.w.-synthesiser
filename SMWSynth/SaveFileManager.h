#pragma once

#include <string>
#include <iostream>
#include <vector>
#include <Windows.h>

#include "DebugLog.h"
#include "Parameters.h"

class CSaveFileManager
{
public:
	CSaveFileManager(void);
	~CSaveFileManager(void);

	bool init();

	std::vector<std::vector<float>> loadBank(std::string filename);
	void saveBank(char* filename, std::vector<std::vector<float>> bank);

	std::vector<float> loadWave(std::string filename);
	void saveWave(char* filename, std::vector<float> wave);

private:
	std::fstream  fsBankFile;
	std::fstream  fsWaveFile;
};

