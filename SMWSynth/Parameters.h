////////////////////////////////////////////////////////////////////////////////
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^//
// Parameters & other enumeration for indexing								  //
// __________________________________________________________________________ //
////////////////////////////////////////////////////////////////////////////////

#ifndef __Parameters__
#define __Parameters__

#define PI 3.1415926535897932384626433832795   // pi given by windows calculator

//// PARAMETERS ////////////////////////////////////////////////////////////////
enum {

	//--Setup------------
	kNumPrograms = 16,
	kNumOutputs = 2,
	kMaxVoices = 48,
	kMaxEvents = 200,

	kEnvMaxAttSecs = 1,
	kEnvMaxDecSecs = 3,
	kEnvMaxRelSecs = 5,

	kWavetableSize = 512,
	kNumMidiNotes  = 128,

	SBK_BANK = 0,
	SWV_WAVE = 1,

	//--Image IDs--------
	kBackgroundID = 1,
	kMasterVolKnobID,
	kSmallerKnobID,
	kTinyKnobID,
	kButtonID,
	kFilterLedID,
	kWaveWinBkgdID,
	kBoxID,
	kButtonNewID,
	kButtonAddID,
	kButtonDelID,
	kButtonLoadBankID,
	kButtonSaveBankID,
	kButtonLoadWaveID,
	kButtonSaveWaveID,
	kUpSelectID,
	kDownSelectID,
	kCtrlDispBkgdID,
	kCtrlDispBkgdLongID,

	//--Params---------
	kVolumeParam = 0,
	kEnvAttParam,
	kEnvDecParam,
	kEnvSusParam,
	kEnvRelParam,
	kBtnNewParam,
	kBtnAddParam,
	kBtnDelParam,
	kBtnLoadBankParam,
	kBtnSaveBankParam,
	kBtnLoadWaveParam,
	kBtnSaveWaveParam,
	kUpSelectParam,
	kDownSelectParam,
	kFilCutParam,
	kFilResParam,
	kFilShape1Param,
	kFilShape2Param,
	kFilShape3Param,
	kFilOffParam,
	kWaveWinParam,

	//--Display Params--
	kBankSizeDisplay,
	kCurrWaveDisplay,
	kFilCutDisplay,
	kFilQDisplay,
	kEnvAttDisplay,
	kEnvDecDisplay,
	kEnvSusDisplay,
	kEnvRelDisplay,

	//--Total Params----
	kNumParameters
};

#endif // Parameters