#pragma once

#include "aeffguieditor.h"
#include "vstgui.h"
#include "cfileselector.h"

#include "SmwButton.h"
#include "WaveWindow.h"
#include "FilterLed.h"
#include "SaveFileManager.h"

#include <iostream>
#include <ole2.h>

class SmwSynthEditor : public AEffGUIEditor, public CControlListener
{
public:
	SmwSynthEditor(void*);

	// from AEffGUIEditor
	bool open (void* ptr);	// called when window is opened
	void close ();			// called when window is closed
	void setParameter (VstInt32 index, float value);

	// from CControlListener
	void valueChanged (CControl* pControl);

	void newWave();
	void addWave();
	void delWave();

	void updateWaveDisplay();

	void runFileSelectorLoad(unsigned int mode);
	void runFileSelectorSave(unsigned int mode);

	bool getBankFromTextFile(char* filename, unsigned int mode);
	bool putBankIntoTextFile(char* filename, unsigned int mode);

	std::vector<std::vector<float>> getWaveBank();

private:
	// array of controls
	CControl* controls[kNumParameters];

	CWaveWindow* wavewindow;

	// file saving
	CSaveFileManager SaveFile;

	// main background
	CBitmap* hBackground;

	std::vector<std::vector<float>> editwavebank;

	int currwave_value;
};

