#include "SmwSynthEditor.h"

//-----------------------------------------------------------------------------
// string convert functions needed for proper representation of values
// within CParamDisplay.

void percentStringConvert (float value, char* string)
{
	 sprintf (string, "%d%%", (int)(100 * value + 0.5f));
}
void integerStringConvert (float value, char* string)
{
	 sprintf (string, "%d", (int)(value));
}
void fcutoffStringConvert (float value, char* string)
{
	sprintf (string, "%.0f Hz", (19980 * ((pow(100,value) - 1)/(100-1)) + 20.0));
}
void fqualityStringConvert (float value, char* string)
{
	float out = value * 10.f;
	if (out < 0.2f) out = 0.2f;
	 sprintf (string, "%.2f", out);
}
void envelopeAttStringConvert (float value, char* string)
{
	float out = value * kEnvMaxAttSecs;
	if (out < 0.01f) out = 0.01f;
	sprintf (string, "%.2f s", out);
}
void envelopeDecStringConvert (float value, char* string)
{
	float out = value * kEnvMaxDecSecs;
	if (out < 0.01f) out = 0.01f;
	sprintf (string, "%.2f s", out);
}
void envelopeRelStringConvert (float value, char* string)
{
	float out = value * kEnvMaxRelSecs;
	if (out < 0.01f) out = 0.01f;
	sprintf (string, "%.2f s", out);
}
void decibelStringConvert (float value, char* string)
{
	float out = value;
	if (out < 0.1) out = 0.1f;
	out = (10 * log(out));
	sprintf (string, "%.1f dB", out);
}

//------------------------------------------------------------------------
AEffGUIEditor* createEditor (AudioEffectX* effect)
{
	return new SmwSynthEditor (effect);
}

//------------------------------------------------------------------------
SmwSynthEditor::SmwSynthEditor (void* ptr) : AEffGUIEditor (ptr)
{
	// Load the background bitmap
	// Others can be loaded within the open function
	hBackground = new CBitmap (kBackgroundID);

	// Set the size of window
	rect.top = 0;
	rect.left = 0;
	rect.right = (short)hBackground->getWidth();
	rect.bottom = (short)hBackground->getHeight();

	editwavebank.clear(); // clear the wavebank vector
	newWave();			  // create a wave to display
	currwave_value = 1;   // initialise the current wave value
}

//------------------------------------------------------------------------
bool SmwSynthEditor::open (void* ptr)
{
	hBackground = new CBitmap (kBackgroundID);

	CColor frame_blue (kBlueCColor);
	CColor bkgd_blue  (kYellowCColor);
	CColor font_col   (kGreenCColor);

	// create the frame and set the background
	CRect frameSize (0, 0, hBackground->getWidth(), hBackground->getHeight());
	CFrame* newFrame = new CFrame (frameSize, ptr, this);
	newFrame->setBackground(hBackground);
	// set the knob mode to linear
	this->setKnobMode(kLinearMode);

	// load the bitmaps
	CBitmap* masterVol			= new CBitmap (kMasterVolKnobID);
	CBitmap* smallerKnob		= new CBitmap (kSmallerKnobID);
	CBitmap* tinyKnob			= new CBitmap (kTinyKnobID);
	CBitmap* button				= new CBitmap (kButtonID);
	CBitmap* led				= new CBitmap (kFilterLedID);
	CBitmap* wavewinbkgd		= new CBitmap (kWaveWinBkgdID);
	CBitmap* box				= new CBitmap (kBoxID);
	CBitmap* buttonNew			= new CBitmap (kButtonNewID);
	CBitmap* buttonAdd			= new CBitmap (kButtonAddID);
	CBitmap* buttonDel			= new CBitmap (kButtonDelID);
	CBitmap* buttonLB			= new CBitmap (kButtonLoadBankID);
	CBitmap* buttonSB			= new CBitmap (kButtonSaveBankID);
	CBitmap* buttonLW			= new CBitmap (kButtonLoadWaveID);
	CBitmap* buttonSW			= new CBitmap (kButtonSaveWaveID);
	CBitmap* upSelect			= new CBitmap (kUpSelectID);
	CBitmap* downSelect			= new CBitmap (kDownSelectID);
	CBitmap* ctrlDispBkgd		= new CBitmap (kCtrlDispBkgdID);
	CBitmap* ctrlDispBkgdLong	= new CBitmap (kCtrlDispBkgdLongID);


	//CNewFileSelector

	// create knobs and add them to the frame
	int numframes = 31; // number of frames is consistent for all knob bitmaps
	// master volume
	CRect r (532, 215, 532+masterVol->getWidth (), 215+masterVol->getHeight ()/numframes);
	CAnimKnob* knob1 = new CAnimKnob (r, this, kVolumeParam, numframes, masterVol->getWidth(), masterVol, CPoint(0,0));
	newFrame->addView (knob1);
	// envelope attack knob
	r (80, 230, 80+smallerKnob->getWidth(), 230+smallerKnob->getHeight()/numframes);
	CAnimKnob* knob2 = new CAnimKnob (r, this, kEnvAttParam, numframes, smallerKnob->getWidth(), smallerKnob, CPoint(0,0));
	newFrame->addView (knob2);
	// envelope decay knob
	r.offset(85,0);
	CAnimKnob* knob3 = new CAnimKnob (r, this, kEnvDecParam, numframes, smallerKnob->getWidth(), smallerKnob, CPoint(0,0));
	newFrame->addView (knob3);
	// envelope sustain knob
	r.offset(85,0);
	CAnimKnob* knob4 = new CAnimKnob (r, this, kEnvSusParam, numframes, smallerKnob->getWidth(), smallerKnob, CPoint(0,0));
	newFrame->addView (knob4);
	// envelope release knob
	r.offset(85,0);
	CAnimKnob* knob5 = new CAnimKnob (r, this, kEnvRelParam, numframes, smallerKnob->getWidth(), smallerKnob, CPoint(0,0));
	newFrame->addView (knob5);

	// setup filter controls here...
	// filter cutoff knob
	r (505,340, 505 + tinyKnob->getWidth(), 340 + tinyKnob->getHeight()/numframes);
	CAnimKnob* knob6 = new CAnimKnob (r, this, kFilCutParam, numframes, tinyKnob->getWidth(), tinyKnob, CPoint(0,0));
	newFrame->addView (knob6);
	// filter resonance knob
	r.offset(0,60);
	CAnimKnob* knob7 = new CAnimKnob (r, this, kFilResParam, numframes, tinyKnob->getWidth(), tinyKnob, CPoint(0,0));
	newFrame->addView (knob7);
	// -- filter buttons
	// shape 1 select
	r (155, 400, 155 + led->getWidth(), 400 + led->getHeight()/2);
	CFilterLed* ledbtn1 = new CFilterLed (r, this, kFilShape1Param, led);
	newFrame->addView (ledbtn1);
	// shape 2 select
	r.offset (85,0);
	CFilterLed* ledbtn2 = new CFilterLed (r, this, kFilShape2Param, led);
	newFrame->addView (ledbtn2);
	// shape 3 select
	r.offset (85,0);
	CFilterLed* ledbtn3 = new CFilterLed (r, this, kFilShape3Param, led);
	newFrame->addView (ledbtn3);
	// off select
	r.offset (85,0);
	CFilterLed* ledbtn4 = new CFilterLed (r, this, kFilOffParam, led);
	newFrame->addView (ledbtn4);

	// create buttons and add them to the frame
	// new button
	r (350, 168, 350 + button->getWidth(), 168 + (button->getHeight()/2));
	CSmwButton* button1 = new CSmwButton (r, this, kBtnNewParam, buttonNew, CPoint(0,0));
	newFrame->addView (button1);
	// add button
	r.offset (55, 0);
	CSmwButton* button2 = new CSmwButton (r, this, kBtnAddParam, buttonAdd, CPoint(0,0));
	newFrame->addView (button2);
	// del button
	r.offset (55, 0);
	CSmwButton* button3 = new CSmwButton (r, this, kBtnDelParam, buttonDel, CPoint(0,0));
	newFrame->addView (button3);
	// load bank button
	r.offset (55, -100);
	CSmwButton* button4 = new CSmwButton (r, this, kBtnLoadBankParam, buttonLB, CPoint(0,0));
	newFrame->addView (button4);
	// save bank button
	r.offset (55, 0);
	CSmwButton* button5 = new CSmwButton (r, this, kBtnSaveBankParam, buttonSB, CPoint(0,0));
	newFrame->addView (button5);
	// load wave button
	r.offset (-55, 55);
	CSmwButton* button6 = new CSmwButton (r, this, kBtnLoadWaveParam, buttonLW, CPoint(0,0));
	newFrame->addView (button6);
	// save wave button
	r.offset (55, 0);
	CSmwButton* button7 = new CSmwButton (r, this, kBtnSaveWaveParam, buttonSW, CPoint(0,0));
	newFrame->addView (button7);

	// wave editor window
	r (80,65, 80 + wavewinbkgd->getWidth(), 65 + wavewinbkgd->getHeight());
	wavewindow = new CWaveWindow (r, this, kWaveWinParam, wavewinbkgd);
	newFrame->addView (wavewindow);

	// up select
	r (400, 80, 400 + upSelect->getWidth(), 80 + (upSelect->getHeight()/2));
	CSmwButton* buttonUp = new CSmwButton (r, this, kUpSelectParam, upSelect, CPoint(0,0));
	newFrame->addView (buttonUp);
	// down select
	r.offset (0, 49);
	CSmwButton* buttonDown = new CSmwButton (r, this, kDownSelectParam, downSelect, CPoint(0,0));
	newFrame->addView (buttonDown);

	// setup value displays
	// bank size
	r (460, 108, 460 + 30, 108 + 15);
	CParamDisplay* banksize = new CParamDisplay (r, 0, kCenterText);
	banksize->setFont(kNormalFontSmall);
	banksize->setFontColor(font_col);
	banksize->setBackground(ctrlDispBkgd);
	banksize->setFrameColor(frame_blue);
	banksize->setStringConvert(integerStringConvert);
	banksize->setValue((float)editwavebank.size());
	newFrame->addView (banksize);
	// current wave
	r.offset (-60,0);
	CParamDisplay* currwave = new CParamDisplay (r, 0, kCenterText);
	currwave->setFont(kNormalFontSmall);
	currwave->setFontColor(font_col);
	currwave->setBackground(ctrlDispBkgd);
	currwave->setFrameColor(frame_blue);
	currwave->setStringConvert(integerStringConvert);
	currwave->setValue((float)currwave_value);
	newFrame->addView (currwave);

	// filter cut off
	r (545, 348, 605, 363);
	CParamDisplay* fil_cutoff_disp = new CParamDisplay (r, 0, kLeftText);
	fil_cutoff_disp->setFont(kNormalFontSmall);
	fil_cutoff_disp->setFontColor(font_col);
	fil_cutoff_disp->setBackground(ctrlDispBkgdLong);
	fil_cutoff_disp->setFrameColor(frame_blue);
	fil_cutoff_disp->setStringConvert(fcutoffStringConvert);
	fil_cutoff_disp->setValue(effect->getParameter(kFilCutParam));
	newFrame->addView (fil_cutoff_disp);
	// filter quality
	r (545, 408, 605, 423);
	CParamDisplay* fil_q_disp = new CParamDisplay (r, 0, kLeftText);
	fil_q_disp->setFont(kNormalFontSmall);
	fil_q_disp->setFontColor(font_col);
	fil_q_disp->setBackground(ctrlDispBkgdLong);
	fil_q_disp->setFrameColor(frame_blue);
	fil_q_disp->setStringConvert(fqualityStringConvert);
	fil_q_disp->setValue(effect->getParameter(kFilResParam));
	newFrame->addView (fil_q_disp);

	// envelope attack time
	r (82, 308, 142, 323);
	CParamDisplay* env_att_disp = new CParamDisplay (r, 0, kLeftText);
	env_att_disp->setFont(kNormalFontSmall);
	env_att_disp->setFontColor(font_col);
	env_att_disp->setBackground(ctrlDispBkgdLong);
	env_att_disp->setFrameColor(frame_blue);
	env_att_disp->setStringConvert(envelopeAttStringConvert);
	env_att_disp->setValue(effect->getParameter(kEnvAttParam));
	newFrame->addView (env_att_disp);
	// envelope decay time
	r.offset (85, 0);
	CParamDisplay* env_dec_disp = new CParamDisplay (r, 0, kLeftText);
	env_dec_disp->setFont(kNormalFontSmall);
	env_dec_disp->setFontColor(font_col);
	env_dec_disp->setBackground(ctrlDispBkgdLong);
	env_dec_disp->setFrameColor(frame_blue);
	env_dec_disp->setStringConvert(envelopeDecStringConvert);
	env_dec_disp->setValue(effect->getParameter(kEnvDecParam));
	newFrame->addView (env_dec_disp);
	// envelope sustain level
	r.offset (85, 0);
	CParamDisplay* env_sus_disp = new CParamDisplay (r, 0, kLeftText);
	env_sus_disp->setFont(kNormalFontSmall);
	env_sus_disp->setFontColor(font_col);
	env_sus_disp->setBackground(ctrlDispBkgdLong);
	env_sus_disp->setFrameColor(frame_blue);
	env_sus_disp->setStringConvert(decibelStringConvert);
	env_sus_disp->setValue(effect->getParameter(kEnvSusParam));
	newFrame->addView (env_sus_disp);
	// envelope release time
	r.offset (85, 0);
	CParamDisplay* env_rel_disp = new CParamDisplay (r, 0, kLeftText);
	env_rel_disp->setFont(kNormalFontSmall);
	env_rel_disp->setFontColor(font_col);
	env_rel_disp->setBackground(ctrlDispBkgdLong);
	env_rel_disp->setFrameColor(frame_blue);
	env_rel_disp->setStringConvert(envelopeRelStringConvert);
	env_rel_disp->setValue(effect->getParameter(kEnvRelParam));
	newFrame->addView (env_rel_disp);
	
	// forget bitmaps
	masterVol->forget();
	smallerKnob->forget();
	tinyKnob->forget();
	button->forget();
	led->forget();
	wavewinbkgd->forget();
	box->forget();
	buttonAdd->forget();
	buttonDel->forget();
	buttonNew->forget();
	buttonLB->forget();
	buttonSB->forget();
	buttonLW->forget();
	buttonSW->forget();
	upSelect->forget();
	downSelect->forget();
	ctrlDispBkgd->forget();
	ctrlDispBkgdLong->forget();

	// remember controls to sync them with the parameters
	controls[kVolumeParam] = knob1;
	controls[kEnvAttParam] = knob2;
	controls[kEnvDecParam] = knob3;
	controls[kEnvSusParam] = knob4;
	controls[kEnvRelParam] = knob5;

	controls[kFilCutParam] = knob6;
	controls[kFilResParam] = knob7;

	controls[kBtnNewParam] = button1;
	controls[kBtnAddParam] = button2;
	controls[kBtnDelParam] = button3;

	controls[kBtnLoadBankParam] = button4;
	controls[kBtnSaveBankParam] = button5;
	controls[kBtnLoadWaveParam] = button6;
	controls[kBtnSaveWaveParam] = button7;

	controls[kFilShape1Param] = ledbtn1;
	controls[kFilShape2Param] = ledbtn2;
	controls[kFilShape3Param] = ledbtn3;
	controls[kFilOffParam]    = ledbtn4;

	controls[kWaveWinParam] = wavewindow;

	controls[kBankSizeDisplay] = banksize;
	controls[kCurrWaveDisplay] = currwave;
	
	controls[kUpSelectParam]   = buttonUp;
	controls[kDownSelectParam] = buttonDown;

	controls[kFilCutDisplay] = fil_cutoff_disp;
	controls[kFilQDisplay]	 = fil_q_disp;

	controls[kEnvAttDisplay] = env_att_disp;
	controls[kEnvDecDisplay] = env_dec_disp;
	controls[kEnvSusDisplay] = env_sus_disp;
	controls[kEnvRelDisplay] = env_rel_disp;

	// set member frame to newly created frame
	frame = newFrame;

	// synchronise parameters
	for (int i = 0; i < kNumParameters; i++)
	{
		setParameter (i, effect->getParameter (i));
	}

	return true;
}


//------------------------------------------------------------------------
void SmwSynthEditor::close ()
{
	// delete the frame object
	// set member frame variable to zero before deleting
	// so calls to setParameter won't crash
	CFrame* oldFrame = frame;
	frame = 0;
	oldFrame->forget ();
}

//------------------------------------------------------------------------
void SmwSynthEditor::valueChanged (CControl* pControl)
{
	// called whenever user changes a control
	effect->setParameterAutomated (pControl->getTag (), pControl->getValue ());
}

//------------------------------------------------------------------------
void SmwSynthEditor::setParameter (VstInt32 index, float value)
{
	// called when host automates a parameter
	// GUI should show this, so the control is updated
	// VSTGUI automaticly redraws changed controls in the next idle 
	if (frame && index < kNumParameters)
	{
		int max = 0;
		switch (index)
		{
		// when a filter shape param is activated, the others need to turn off
		case kFilShape1Param:
			controls[kFilShape1Param]->setValue (value);
			controls[kFilShape2Param]->setValue (0.f);
			controls[kFilShape3Param]->setValue (0.f);
			controls[kFilOffParam]->setValue (0.f);
			break;
		case kFilShape2Param:
			controls[kFilShape1Param]->setValue (0.f);
			controls[kFilShape2Param]->setValue (value);
			controls[kFilShape3Param]->setValue (0.f);
			controls[kFilOffParam]->setValue (0.f);
			break;
		case kFilShape3Param:
			controls[kFilShape1Param]->setValue (0.f);
			controls[kFilShape2Param]->setValue (0.f);
			controls[kFilShape3Param]->setValue (value);
			controls[kFilOffParam]->setValue (0.f);
			break;
		case kFilOffParam:
			controls[kFilShape1Param]->setValue (0.f);
			controls[kFilShape2Param]->setValue (0.f);
			controls[kFilShape3Param]->setValue (0.f);
			controls[kFilOffParam]->setValue (value);
			break;

		// wave controls here
		case kBtnNewParam: if (value > 0.f) newWave(); 
			currwave_value = editwavebank.size(); break; // creates a new wave filled with a sine wave
		case kBtnAddParam: if (value > 0.f) addWave(); break; // updates the wavebank from window 
		case kBtnDelParam: if (value > 0.f) delWave(); break; // deletes the selected wave from the bank break;
		
		// load/save controls here 
		// - functions execute an 'open/save file' window to get / put data into relevant files
		case kBtnLoadBankParam: if (value > 0.f) runFileSelectorLoad (SBK_BANK); break;
		case kBtnSaveBankParam: if (value > 0.f) runFileSelectorSave (SBK_BANK); break;
		case kBtnLoadWaveParam: if (value > 0.f) runFileSelectorLoad (SWV_WAVE); break;
		case kBtnSaveWaveParam: if (value > 0.f) runFileSelectorSave (SWV_WAVE); break;
		
		case kUpSelectParam:
			max = (int)effect->getParameter(kWaveWinParam); // get the current bank size
			if (currwave_value < max && value != 0.f) // as long as the current value is less
			{										  // and we have a positive value
				currwave_value++;					  // increment the current wave index
			}
			break;
		case kDownSelectParam:
			if (currwave_value > 1 && value != 0.f) // as long as the current wave is above 1
			{										// theres another wave at the index below
				currwave_value--;					// so decrease the value
			}
			break;

		// when these are called, nothing needs to be done
		case kCurrWaveDisplay: break;
		case kBankSizeDisplay: break; 
		case kWaveWinParam:    break;

		// if none of the above are selected...
		default: controls[index]->setValue (value); break;
		}

		// update the display parameters
		// - happens on every call to setParameter
		controls[kCurrWaveDisplay]->setValue ((float)currwave_value);
		controls[kBankSizeDisplay]->setValue (effect->getParameter(kWaveWinParam));

		controls[kFilCutDisplay]->setValue (effect->getParameter(kFilCutParam));
		controls[kFilQDisplay]->setValue (effect->getParameter(kFilResParam));

		controls[kEnvAttDisplay]->setValue(effect->getParameter(kEnvAttParam));
		controls[kEnvDecDisplay]->setValue(effect->getParameter(kEnvDecParam));
		controls[kEnvSusDisplay]->setValue(effect->getParameter(kEnvSusParam));
		controls[kEnvRelDisplay]->setValue(effect->getParameter(kEnvRelParam));

		controls[kWaveWinParam]->setDirty();
		updateWaveDisplay();
	}
}

//------------------------------------------------------------------------------------
void SmwSynthEditor::newWave ()
{
	std::vector<float> temp;	// create a temporary table
	float windowScaler = -70.f; // number to scale output by to fit in wave display

	for (int i = 0; i < kWavetableSize; i++)
	{
		// fill it with a sine wave
		temp.push_back(sin(((float)i/kWavetableSize)*(2.f*PI))*windowScaler);
	}
	editwavebank.push_back(temp);	// add it to the bank
}
//------------------------------------------------------------------------------------
void SmwSynthEditor::addWave ()
{
	// store the values from the wavewindow control in the wave bank
	editwavebank.at(currwave_value-1) = wavewindow->getWindowPoints();

	if (editwavebank.at(currwave_value-1).size() != kWavetableSize)
		LogFile.post("addWave: size doesn't match!\n");
}
//------------------------------------------------------------------------------------
void SmwSynthEditor::delWave ()
{
	if (editwavebank.size() > 1) // if there's waves in the bank
	{
		editwavebank.erase(editwavebank.begin() + currwave_value-1);	// erase the current wave
		
		if (currwave_value > 1) currwave_value--; // decrease the current wave value
	}
}

//------------------------------------------------------------------------------------
void SmwSynthEditor::updateWaveDisplay ()
{
	// pass the current wave vector to the display
	wavewindow->setWindowPoints(editwavebank.at(currwave_value-1));
}

//------------------------------------------------------------------------------------
std::vector<std::vector<float>> SmwSynthEditor::getWaveBank ()
{
	// create a temporary wavebank to edit
	std::vector<std::vector<float>> temp_bank;
	temp_bank.clear();	// make sure its empty
	temp_bank = editwavebank;  // copy contents of wavebank
	int bank_size = editwavebank.size();  // set the bank size

	if (bank_size > 0)	// if there are waves to process
	{
		for (int i = 0; i < bank_size; i++)	// iterate through bank
		{
			for (int j = 0; j < kWavetableSize; j++) // iterate through waveform
			{
				// modify values to fit within range -1 to 1
				temp_bank.at(i).at(j) /= 75;
			}
		}
	}
	// return audio-adapted wavebank
	return temp_bank;
}

//-------------------------------------------------------------------------
void SmwSynthEditor::runFileSelectorLoad (unsigned int mode)
{
#ifdef WIN32     
	if (mode == SBK_BANK)
	{
        OPENFILENAME lpofn;	// long pointer to an OPENFILENAME struct
		// this is passed to the GetOpenFileName() windows function to receive info

        TCHAR lpwFileName[MAX_PATH];	// wide char array for filename
        lpwFileName[0] = '\0';			// init the array, must be null-terminated
         
        lpofn.lStructSize		= sizeof (OPENFILENAME); // define exact size
        lpofn.hwndOwner			= NULL; 
        lpofn.hInstance			= NULL; 
		// this is the selectable file extension option in the drop-down selector
        lpofn.lpstrFilter		= TEXT("Sequent Bank Files (*.sbk)\0*.sbk;*.sbk\0\0");;
        lpofn.lpstrCustomFilter = NULL; 
        lpofn.nMaxCustFilter	= NULL; 
        lpofn.nFilterIndex		= NULL; 
		// the full path of the selected file. we set a pointer to its location
        lpofn.lpstrFile			= lpwFileName;
		// the maximum length a path can be, defined in WinDef.h
        lpofn.nMaxFile			= MAX_PATH; 
        lpofn.lpstrFileTitle	= NULL; 
        lpofn.nMaxFileTitle		= NULL; 
		// the initial directory to open, if NULL we open current directory
        lpofn.lpstrInitialDir	= NULL;
		// the message to display in the title bar of the window
        lpofn.lpstrTitle		= TEXT("Load Sequent Bank File"); 
		// some options to define, enable sizing of the window and to hide read only files
        lpofn.Flags				= OFN_ENABLESIZING | OFN_HIDEREADONLY;
        lpofn.nFileOffset		= NULL;
        lpofn.nFileExtension	= NULL;
		// the default file extension
        lpofn.lpstrDefExt		= TEXT("sbk");
        lpofn.lCustData			= NULL;
        lpofn.lpfnHook			= NULL;
        lpofn.lpTemplateName	= NULL;

        if (GetOpenFileName(&lpofn))
        {
			if (lpwFileName != 0)
			{
				std::wstring wsFileName (lpwFileName);  // store TCHAR array in wide string
				std::string  sFileName;					// standard string to pass to function
				sFileName.assign(wsFileName.begin(),wsFileName.end()); // convert wide->standard

				// now we have file location we can read the file
				if (!(getBankFromTextFile( &sFileName[0] , mode)))
					MessageBox(NULL, lpwFileName, TEXT("Error loading bank file"), MB_OK);
				else
					MessageBox(NULL, lpwFileName, TEXT("Loaded Bank"), MB_OK);
			}
			else LogFile.post("Filename empty!\n"); // if the filename doesnt contain any data
        }
	}
	else if (mode == SWV_WAVE)
	{
		 OPENFILENAME lpofn;	// long pointer to an OPENFILENAME struct
		// this is passed to the GetOpenFileName() windows function to receive info

        TCHAR lpwFileName[MAX_PATH];	// wide char array for filename
        lpwFileName[0] = '\0';			// init the array, must be null-terminated
         
        lpofn.lStructSize		= sizeof (OPENFILENAME);
        lpofn.hwndOwner			= NULL; 
        lpofn.hInstance			= NULL; 
        lpofn.lpstrFilter		= TEXT("Sequent Wave Files (*.sbkw)\0*.sbkw;*.sbkw\0\0");;
        lpofn.lpstrCustomFilter = NULL; 
        lpofn.nMaxCustFilter	= NULL; 
        lpofn.nFilterIndex		= NULL; 
        lpofn.lpstrFile			= lpwFileName;
        lpofn.nMaxFile			= MAX_PATH; 
        lpofn.lpstrFileTitle	= NULL; 
        lpofn.nMaxFileTitle		= NULL; 
        lpofn.lpstrInitialDir	= NULL; 
        lpofn.lpstrTitle		= TEXT("Load Sequent Wave File"); 
        lpofn.Flags				= OFN_ENABLESIZING | OFN_HIDEREADONLY;
        lpofn.nFileOffset		= NULL;
        lpofn.nFileExtension	= NULL;
        lpofn.lpstrDefExt		= TEXT("sbkw");
        lpofn.lCustData			= NULL;
        lpofn.lpfnHook			= NULL;
        lpofn.lpTemplateName	= NULL;

        if (GetOpenFileName(&lpofn))
        {
			if (lpwFileName != 0)
			{
				std::wstring wsFileName (lpwFileName);  // store TCHAR array in wide string
				std::string  sFileName;					// standard string to pass to function
				sFileName.assign(wsFileName.begin(),wsFileName.end()); // convert wide->standard

				// now we have file location we can read the file
				if (!(getBankFromTextFile( &sFileName[0] , mode)))
					MessageBox(NULL, lpwFileName, TEXT("Error loading wave file"), MB_OK);
				else
					MessageBox(NULL, lpwFileName, TEXT("Loaded Current Wave"), MB_OK);
			}
			else LogFile.post("Filename empty!\n"); // if the filename doesnt contain any data
        }
	}
#endif      
#if MAC
	if (mode == SBK_BANK)
	{
		char szPathName[MAX_PATH+1];
		strcpy(szPathName, "*.sbk");
		if (MacOpenFileDlg(CFSTR(APPNAME), CFSTR("Load Sequent Bank File (*.sbk) file"), szPathName, MAX_PATH))
		{
			// got filename, now we can operate on it
			if (!(getBankFromTextFile(szPathName, mode))
			{
				SInt16 outItemHit;
				StandardAlert(kAlertStopAlert, P_APPNAME, "\pError loading bank!", NULL, &outItemHit);
			}
		}
	}
	else if (mode == SWV_WAVE)
	{
		char szPathName[MAX_PATH+1];
		strcpy(szPathName, "*.sbkw");
		if (MacOpenFileDlg(CFSTR(APPNAME), CFSTR("Load Sequent Wave File (*.sbkw) file"), szPathName, MAX_PATH))
		{
			// got filename, now we can operate on it
			if (!(getBankFromTextFile(szPathName, mode))
			{
				SInt16 outItemHit;
				StandardAlert(kAlertStopAlert, P_APPNAME, "\pError loading wave!", NULL, &outItemHit);
			}
		}
	}
#endif
}

//-------------------------------------------------------------------------
void SmwSynthEditor::runFileSelectorSave (unsigned int mode)
{
#ifdef WIN32    
	if (mode == SBK_BANK)
	{
        OPENFILENAME lpofn;	// long pointer to an OPENFILENAME struct
		// this is passed to the GetOpenFileName() windows function to receive info

        TCHAR lpwFileName[MAX_PATH];	// wide char array for filename
        lpwFileName[0] = '\0';			// init the array, must be null-terminated
         
        lpofn.lStructSize		= sizeof (OPENFILENAME);
        lpofn.hwndOwner			= NULL; 
        lpofn.hInstance			= NULL; 
        lpofn.lpstrFilter		= TEXT("Sequent Bank Files (*.sbk)\0*.sbk;*.sbk\0\0");;
        lpofn.lpstrCustomFilter = NULL; 
        lpofn.nMaxCustFilter	= NULL; 
        lpofn.nFilterIndex		= NULL; 
        lpofn.lpstrFile			= lpwFileName;
        lpofn.nMaxFile			= MAX_PATH; 
        lpofn.lpstrFileTitle	= NULL; 
        lpofn.nMaxFileTitle		= NULL; 
        lpofn.lpstrInitialDir	= NULL; 
        lpofn.lpstrTitle		= TEXT("Save Sequent Bank File"); 
        lpofn.Flags				= OFN_ENABLESIZING | OFN_HIDEREADONLY;
        lpofn.nFileOffset		= NULL;
        lpofn.nFileExtension	= NULL;
        lpofn.lpstrDefExt		= TEXT("sbk");
        lpofn.lCustData			= NULL;
        lpofn.lpfnHook			= NULL;
        lpofn.lpTemplateName	= NULL;

        if (GetSaveFileName(&lpofn))
        {
			if (lpwFileName != 0)
			{
				std::wstring wsFileName (lpwFileName);	// store TCHAR array in a wide string
				std::string  sFileName;					// standard string to pass to function
				sFileName.assign(wsFileName.begin(),wsFileName.end()); // convert wide->standard

				// supply the save function with correct file name to save the bank
				if (!(putBankIntoTextFile( &sFileName[0] , mode)))
					MessageBox(NULL, lpwFileName, TEXT("Error saving bank file"), MB_OK);
				else
					MessageBox(NULL, lpwFileName, TEXT("Saved Bank"), MB_OK);
			}
			else LogFile.post("Filename empty!\n"); // if the filename doesnt contain any data
        }
	}
	else if (mode == SWV_WAVE)
	{
		OPENFILENAME lpofn;	// long pointer to an OPENFILENAME struct
		// this is passed to the GetOpenFileName() windows function to receive info

        TCHAR lpwFileName[MAX_PATH];	// wide char array for filename
        lpwFileName[0] = '\0';			// init the array, must be null-terminated
         
        lpofn.lStructSize		= sizeof (OPENFILENAME);
        lpofn.hwndOwner			= NULL; 
        lpofn.hInstance			= NULL; 
        lpofn.lpstrFilter		= TEXT("Sequent Wave Files (*.sbkw)\0*.sbkw;*.sbkw\0\0");;
        lpofn.lpstrCustomFilter = NULL; 
        lpofn.nMaxCustFilter	= NULL; 
        lpofn.nFilterIndex		= NULL; 
        lpofn.lpstrFile			= lpwFileName;
        lpofn.nMaxFile			= MAX_PATH; 
        lpofn.lpstrFileTitle	= NULL; 
        lpofn.nMaxFileTitle		= NULL; 
        lpofn.lpstrInitialDir	= NULL;
        lpofn.lpstrTitle		= TEXT("Save Sequent Wave File"); 
        lpofn.Flags				= OFN_ENABLESIZING | OFN_HIDEREADONLY;
        lpofn.nFileOffset		= NULL;
        lpofn.nFileExtension	= NULL;
        lpofn.lpstrDefExt		= TEXT("sbkw");
        lpofn.lCustData			= NULL;
        lpofn.lpfnHook			= NULL;
        lpofn.lpTemplateName	= NULL;

        if (GetSaveFileName(&lpofn))
        {
			if (lpwFileName != 0)
			{
				std::wstring wsFileName (lpwFileName);	// store TCHAR array in a wide string
				std::string  sFileName;					// standard string to pass to function
				sFileName.assign(wsFileName.begin(),wsFileName.end()); // convert wide->standard

				// supply the save function with correct file name and mode to save the wave
				if (!(putBankIntoTextFile( &sFileName[0] , mode)))
					MessageBox(NULL, lpwFileName, TEXT("Error saving wave file"), MB_OK);
				else
					MessageBox(NULL, lpwFileName, TEXT("Saved Current Wave"), MB_OK);
			}
			else LogFile.post("Filename empty!\n"); // if the filename doesnt contain any data
        }
	}
#endif      
#if MAC
	if (mode == SBK_BANK)
	{
		char szPathName[MAX_PATH+1];
		strcpy(szPathName, "*.sbk");
		if (MacOpenFileDlg(CFSTR(APPNAME), CFSTR("Save Sequent Bank File (*.sbk) file"), szPathName, MAX_PATH))
		{
			if (!(putBankIntoTextFile(szPathName, mode))
			{
				SInt16 outItemHit;
				StandardAlert(kAlertStopAlert, P_APPNAME, "\pError saving bank!", NULL, &outItemHit);
			}
		}
	}
	else if (mode == SWV_WAVE)
	{
		char szPathName[MAX_PATH+1];
		strcpy(szPathName, "*.sbkw");
		if (MacOpenFileDlg(CFSTR(APPNAME), CFSTR("Save Sequent Wave File (*.sbkw) file"), szPathName, MAX_PATH))
		{
			if (!(putBankIntoTextFile(szPathName, mode))
			{
				SInt16 outItemHit;
				StandardAlert(kAlertStopAlert, P_APPNAME, "\pError saving wave!", NULL, &outItemHit);
			}
		}
	}
#endif
}

//-------------------------------------------------------------------------
bool SmwSynthEditor::getBankFromTextFile(char* filename,unsigned int mode)
{
	std::vector<std::vector<float>> tempbank; // temp bank / wave so we can check
	std::vector<float> tempwave; // contents before updating the editor wavebank
	tempbank.clear(); // make sure they are empty
	tempwave.clear();

	switch (mode)
	{
	case SBK_BANK:
		// execute load bank function, updating tempbank with returned vector
		tempbank = SaveFile.loadBank(filename);
	
		// check to see if the bank has data
		if (tempbank.size() > 0)
		{
			currwave_value = 1; // reset the current wave (in-case we only have one!)

			// update the plugin editor's wavebank
			editwavebank = tempbank;

			// update the wavewindow
			updateWaveDisplay();

			return true;
		} 
		break;

	case SWV_WAVE:
		// execute load wave function, updating tempwave with returned vector
		tempwave = SaveFile.loadWave(filename);
	
		// check to see if the wave has data
		if (tempwave.size() > 0)
		{
			// update the plugin editor's wavebank
			editwavebank.at(currwave_value-1) = tempwave;

			// update the wavewindow
			updateWaveDisplay();

			return true;
		} 
		break;
	}
	return false; // if we reach here we don't have any data
}

//-------------------------------------------------------------------------
bool SmwSynthEditor::putBankIntoTextFile(char* filename, unsigned int mode)
{
	switch (mode)
	{
	case SBK_BANK:
		// execute save bank function, supplying editwavebank as param
		SaveFile.saveBank(filename, editwavebank);
		break;
	case SWV_WAVE:
		// execute save wave function, supplying editwavebank as param
		SaveFile.saveWave(filename, editwavebank.at(currwave_value-1));
		break;
	}
	return true;
}