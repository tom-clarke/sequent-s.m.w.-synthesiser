#include "OscManager.h"


COscManager::COscManager(void)
{
	for (int i = 0; i < kMaxVoices; i++)
	{
		active_notes[i] = -1;
	}
}

COscManager::~COscManager(void)
{
	
}

void COscManager::setSampleRate	(float sr)
{
	for (int i = 0; i < kMaxVoices; i++)
	{
		oscillators[i].setSampleRate(sr);
	}
}

void COscManager::setWavebank (std::vector<std::vector<float>> bank)
{
	unsigned int i, j;
	std::vector<float> signal;
	signal.clear();

	for (i = 0; i < bank.size(); i++)
	{
		for (j = 0; j < bank.at(i).size(); j++)
		{
			signal.push_back(bank.at(i).at(j));
		}
	}

	for (i = 0; i < kMaxVoices; i++)
	{
		oscillators[i].setWaveSignal(signal);
	}
}

void COscManager::setADSR (float a, float d, float s, float r)
{
	float att, dec, sus, rel;
	att = dec = sus = rel = 0.f;

	// cook values from 0.0-1.0 to 0.1-kEnvMax###Secs
	// so secs cannot be zero
	if (a < 0.01f) att = 0.01f; else att = a * kEnvMaxAttSecs;
	if (d < 0.01f) dec = 0.01f; else dec = d * kEnvMaxDecSecs;
	if (s < 0.1f)  sus = 0.1f;  else sus = s;
	if (r < 0.01f) rel = 0.01f; else rel = r * kEnvMaxRelSecs;

	for (int i = 0; i < kMaxVoices; i++)
	{
		oscillators[i].setADSR(att,dec,sus,rel);
	}
}

void COscManager::noteOn(int note, int velocity)
{
	int i;

	if (note < 0) // any value below 0 is an
		return; // invalid note, exit function

	for (i = 0; i < kMaxVoices; i++)
	{
		if (!oscillators[i].isPlaying()) // find free oscillator
		{
			oscillators[i].noteOn(note,velocity); // play the osc
			active_notes[i] = note;	// remember the note
			return; // exit function
		}
	}
	LogFile.post("didnt find any free oscs!\n");
	// if no oscillators are free the one with the lowest output value
	// is found and this one is used to trigger the new note
	float cur_val = 0.f, min_val = 0.f;
	int min_index = 0;

	for (i = 0; i < kMaxVoices; i++) // loop through again
	{
		// get the last output value
		cur_val = oscillators[i].getOutput();
		if (cur_val < min_val) // if its less than the min so far
		{
			min_val = cur_val; // store the minimum value
			min_index = i;	   // and the oscillator index
		}
	}
	oscillators[min_index].turnOff();
	oscillators[min_index].noteOn(note,velocity); // turn on the osc
	active_notes[min_index] = note; // remember which note it was
}
	
void COscManager::noteOff(int note)
{
	if (note < 0) // can't have negative notes
	{
		if (note == -1) reset(); // -1 = all notes off
		return; // exit function
	}

	for (int i = 0; i < kMaxVoices; i++) // iterate through active notes
	{
		if (note == active_notes[i])
			oscillators[i].noteOff(); // start the release curve
	}
}

void COscManager::reset()
{
	for (int i = 0; i < kMaxVoices; i++) // iterate through active oscs
	{
		oscillators[i].turnOff(); // stop sound immediately and reset
	}
}

bool COscManager::isPlaying()
{
	for (int i = 0; i < kMaxVoices; i++) // iterate through active notes
	{
		if (oscillators[i].isPlaying()) // if any oscs are playing
			return true; // respond positively
	}
	return false; // nothing was playing
}

float COscManager::getSample()
{
	float output = 0.f;

	for (int i = 0; i < kMaxVoices; i++) // iterate through active notes
	{
		if (oscillators[i].isPlaying()) // if any oscs are playing
			output += oscillators[i].getSample(); // get sample
	}
	return output / 4.f; // reduce amplitude due to mupltiple oscs
}

// this checks numbers to see if they are valid
bool COscManager::isNumber(double x) 
{
    // This looks like it should always be true, 
    // but it's false if x is a NaN.
    return (x == x); 
}