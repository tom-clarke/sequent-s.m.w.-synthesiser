#pragma once

#include "vstgui.h"
#include <vector>
#include "Parameters.h"
#include "DebugLog.h"
#include "Interp.h"

enum {
	kWaveWinTableLength = 256,
	kWaveWinTableHeight = 150,

	kWaveWinX = 80,
	kWaveWinY = 65,

	kFadeLength = 4
};

class CWaveWindow : public CControl
{
public:
	CWaveWindow(const CRect &size, CControlListener *listener, long tag, 
		CBitmap *pBackground);	// bitmap of the background

	virtual ~CWaveWindow();

	virtual void draw(CDrawContext *pContext);
	void drawWave(CDrawContext *pContext);

	virtual CMouseEventResult onMouseDown (CPoint &where, const long& buttons);		// called when a mouse down event occurs
	virtual CMouseEventResult onMouseUp (CPoint &where, const long& buttons);		// called when a mouse up event occurs
	virtual CMouseEventResult onMouseMoved (CPoint &where, const long& buttons);	// called when a mouse move event occurs

	virtual CMouseEventResult onMouseEntered (CPoint &where, const long& buttons) {return kMouseEventNotImplemented;}	// called when the mouse enters this view
	virtual CMouseEventResult onMouseExited  (CPoint &where, const long& buttons) {return kMouseEventNotImplemented;}	// called when the mouse leaves this view
	
	std::vector<float> getWindowPoints();
	void setWindowPoints(std::vector<float> newpoints);
	
	void applyFade();

	CLASS_METHODS(CWaveWindow, CControl)

private:
	CPoint firstPoint;
	CPoint lastPoint;
	long   oldButton;

	CPoint mouseDown;

	std::vector<float> winpoints;
};
