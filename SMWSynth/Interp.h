#pragma once

#include "vstgui.h"

//// INTERPOLATION ////

//// Linear ////////////////////////////////////////////////////////
float linearInterp (float val1, float val2, float target);

//// Lagrange //////////////////////////////////////////////////////
/******************************************************************
//	output(target) =  (  (target - x[1]) / (x[0] - x[1]) 
//						*(target - x[2]) / (x[0] - x[2]) 
//						*(target - x[3]) / (x[0] - x[3])  ) * y[0]
//					+	
//	The calc		  (  (target - x[0]) / (x[1] - x[0])
//	worked				*(target - x[2]) / (x[1] - x[2])
//	out					*(target - x[3]) / (x[1] - x[3])  ) * y[1]
//					+
//					  (	 (target - x[0]) / (x[2] - x[0])
//						*(target - x[1]) / (x[2] - x[1])
//						*(target - x[3]) / (x[2] - x[3])  ) * y[2]
//					+
//					  (	 (target - x[0]) / (x[3] - x[0])
//						*(target - x[1]) / (x[3] - x[1])
//						*(target - x[2]) / (x[3] - x[2])  ) * y[3]
*******************************************************************/
float lagrangeInterp (float val1, float val2, float val3, float val4, float target);
float lagrangeInterp (CPoint loc1, CPoint loc2, CPoint loc3, CPoint loc4, float target);

