#include "BQFilter.h"

//-------------------------------------------------------------------
CBQFilter::CBQFilter(void)
{
	a0 = 1.0;
	a1 = a2 = b0 = b1 = z1 = z2 = 0.0;
	
	filterType = FTYPE_OFF;
	sampleRate = 44100.0;
	centerFreq = 20000.0 / sampleRate; // around 20kHz
	Q = 0.707;
}

//-------------------------------------------------------------------
CBQFilter::~CBQFilter(void)
{

}

//-------------------------------------------------------------------
void CBQFilter::setFilter(int type, double freq, double q, double sr)
{
	sampleRate = sr;
	filterType = type;
	centerFreq = freq / sampleRate;
	Q = q;
	calculate();
}

//-------------------------------------------------------------------
void CBQFilter::setCenterFreq(double freq)
{
	double frequency = (19980 * ((pow(100, freq) - 1)/(100-1)) + 20.0);
	centerFreq = frequency / sampleRate;
	calculate();
}

//-------------------------------------------------------------------
void CBQFilter::setQuality(double q)
{
	if (q < 0.2) Q = 0.2; else Q = q * 10.0;
	calculate();
}

//-------------------------------------------------------------------
void CBQFilter::setType(int type)
{
	filterType = type;
	calculate();
}

//-------------------------------------------------------------------
void CBQFilter::calculate()
{
	double normal, k;
	normal = 0.0;
	k      = tan ( PI * centerFreq );
	normal = 1.0 / ( 1.0 + k / Q + k * k );

	switch (filterType)
	{
	case FTYPE_LOW_PASS:
		a0 = k * k * normal;
		a1 = 2.0 * a0;
		a2 = a0;
		b0 = 2.0 * ( k * k - 1.0 ) * normal;
		b1 = ( 1.0 - k / Q + k * k ) * normal;
		break;
	case FTYPE_BAND_STOP:
		a0 = ( 1.0 + k * k ) * normal;
		a1 = 2.0 * ( k * k - 1.0 ) * normal;
		a2 = a0;
		b0 = a1;
		b1 = ( 1.0 - k / Q + k * k ) * normal;
		break;
	case FTYPE_HIGH_PASS:
		a0 = normal;
		a1 = -2.0 * a0;
		a2 = a0;
		b0 = 2.0 * ( k * k - 1.0 ) * normal;
		b1 = ( 1.0 - k / Q + k * k ) * normal;
		break;
	}
	return;
}

//-------------------------------------------------------------------