#pragma once

#include <math.h>
#include "Parameters.h"
#include "DebugLog.h"

//-------------------------------------------------------------------
enum {
	FTYPE_OFF = 0,
	FTYPE_LOW_PASS,
	FTYPE_HIGH_PASS,
	FTYPE_BAND_STOP
};

//-------------------------------------------------------------------
class CBQFilter
{
public:
	CBQFilter(void);
	~CBQFilter(void);

	void setFilter(int type, double freq, double q, double sr);
	void setSampleRate(double srate);
	void setCenterFreq(double freq);
	void setQuality(double q);
	void setType(int type);
	
	float getSample(float input);
private:
	void calculate();

	double a0, a1, a2, b0, b1; // the coefficients
	double centerFreq, Q;	   // input parameters
	double z1, z2;             // the delayed samples
	double sampleRate;		   // the host sample rate

	int filterType;
};

//-------------------------------------------------------------------
inline float CBQFilter::getSample(float input)
{
	double output = 0.0;
	if (filterType != FTYPE_OFF)
	{
		output = input * a0 + z1;
		z1	   = input * a1 + z2 - b1 * output;
		z2	   = input * a2 - b1 * output;
	}
	else output = input;
	return (float) output;
}