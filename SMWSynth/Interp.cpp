#include "Interp.h"

//// Linear interpolation ///////////////////////////////////////////////////
float linearInterp(float val1, float val2, float target)
{
	if (target == 0.f) return val1;
	if (target == 1.f) return val2;
	return (val1 * (1 - target) + val2 * target);
}

//// Lagrange interpolation /////////////////////////////////////////////////
float lagrangeInterp(float val1, float val2, float val3, float val4, float target)
{
	int i, j;  // iterators
	int N = 4; // the order of interpolation (and the amount of points in use)

	float x[4] = {0.0, 1.0, 2.0, 3.0}; // target will be between 0.0 and 1.0
	float y[4] = {val1, val2, val3, val4};   // our y values

	double output = 0.0;

	for (i = 0; i < N; i++)
	{
		double temp = 1.f;
		for(j = 0; j < N; j++)
		{
			if (j != i) 
			{
				temp = temp * (target - x[j]) / (x[i] - x[j]);
			}
		}
		temp *= y[i];
		output += temp;
	}

	return (float) output;
}

float lagrangeInterp (CPoint loc1, CPoint loc2, CPoint loc3, CPoint loc4, float target)
{
	int i, j;  // iterators
	int N = 4; // the order of interpolation (and the amount of points in use)

	float x[4] = {loc1.x, loc2.x, loc3.x, loc4.x}; // target will be between 0.0 and 1.0
	float y[4] = {loc1.y, loc2.y, loc3.y, loc4.y}; // our y values

	double output = 0.0;

	for (i = 0; i < N; i++)
	{
		double temp = 1.f;
		for(j = 0; j < N; j++)
		{
			if (j != i) 
			{
				temp = temp * (target - x[j]) / (x[i] - x[j]);
			}
		}
		temp *= y[i];
		output += temp;
	}

	return (float) output;
}