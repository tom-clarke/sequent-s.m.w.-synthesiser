#pragma once

#include <vector>

#include "TabOsc.h"
#include "Parameters.h"

class COscManager
{
public:
	COscManager(void);
	~COscManager(void);

	void setSampleRate	(float sr);
	void setWavebank	(std::vector<std::vector<float>> bank);
	void setADSR		(float a, float d, float s, float r);

	void noteOn(int note, int velocity);
	void noteOff(int note);
	void reset();

	bool isPlaying();

	float getSample();

private:
	CTabOsc oscillators[kMaxVoices];
	int	active_notes[kMaxVoices];

	bool isNumber(double x);
};

