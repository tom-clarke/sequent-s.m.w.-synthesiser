#include "SaveFileManager.h"

//-----------------------------------------------------------------------------------
CSaveFileManager::CSaveFileManager(void)
{

}

//-----------------------------------------------------------------------------------
CSaveFileManager::~CSaveFileManager(void)
{
	if (fsBankFile)     fsBankFile.close();
	if (fsWaveFile)		fsWaveFile.close();
}

//-----------------------------------------------------------------------------------
std::vector<std::vector<float>> CSaveFileManager::loadBank(std::string filename)
{
	fsBankFile.open(filename, std::fstream::in);

	int   bankSize  = 0;
	int   currWave  = 0, lastWave = 0;
	int   currIndex = 0;
	float currValue = 0.f;
	std::vector<float> tempwave;
	std::vector<std::vector<float>> tempbank;
	tempwave.clear();
	tempbank.clear();

	fsBankFile >> bankSize;

	while (fsBankFile >> currWave)
	{
		fsBankFile >> currIndex;
		fsBankFile >> currValue;

		if (lastWave < currWave)
		{
			tempbank.push_back(tempwave);
			tempwave.clear();
		}
		tempwave.push_back(currValue);

		lastWave = currWave;
	}
	tempbank.push_back(tempwave);

	if (tempbank.size() != bankSize) LogFile.post("load bank: size mismatch\n");

	fsBankFile.close();

	return tempbank;
}

//-----------------------------------------------------------------------------------
void CSaveFileManager::saveBank(char* filename, std::vector<std::vector<float>> bank)
{

	fsBankFile.open(filename, std::fstream::out);

	fsBankFile << bank.size() << "\n";
	
	for (unsigned int i = 0; i < bank.size(); i ++)
	{
		for (unsigned int j = 0; j < bank.at(i).size(); j ++)
		{
			fsBankFile << i << "\t" << j << "\t" << bank.at(i).at(j) << "\n";
		}
	}

	fsBankFile.close();
}

//-----------------------------------------------------------------------------------
std::vector<float> CSaveFileManager::loadWave(std::string filename)
{
	fsWaveFile.open(filename, std::fstream::in);

	int   currIndex = 0;
	float currValue = 0.f;
	std::vector<float> tempwave;
	tempwave.clear();

	while (fsWaveFile >> currIndex)
	{
		fsWaveFile >> currValue;

		tempwave.push_back(currValue);
	}

	if (tempwave.size() != kWavetableSize) LogFile.post("load wave: size mismatch!\n");

	fsWaveFile.close();

	return tempwave;
}

//-----------------------------------------------------------------------------------
void CSaveFileManager::saveWave(char* filename, std::vector<float> wave)
{
	fsWaveFile.open(filename, std::fstream::out);	// open the file for writing
	
	for (unsigned int i = 0; i < wave.size(); i ++)
	{
		fsWaveFile << i  << "\t" << wave.at(i) << "\n";
	}

	fsWaveFile.close();
}

//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------