#include "DebugLog.h"

bool CDebugLog::init()
{
	curr_time = 0.0; // initialise time variable

	logfile.open("smwsynth.log"); // open the log file
	if(!logfile) return false;	// test for success

	return true;
}

void CDebugLog::timestamp()
{
	// divide the clocks passed by the clocks per sec
	// to get seconds elasped since program execution
	curr_time = clock()/static_cast<double> (CLOCKS_PER_SEC);
}

void CDebugLog::post(char* msg)
{
	if (logfile.is_open()) // check the file is open
	{
		timestamp(); // store elapsed time
		logfile << "time:" << curr_time << "\t" << msg; // write the message to file
	}
}

void CDebugLog::post(std::wstring msg)
{
	if (wlogfile.is_open()) // check the file is open
	{
		timestamp(); // store elapsed time
		wlogfile << "time:" << curr_time << "\t" << &msg; // write the message to file
	}
}