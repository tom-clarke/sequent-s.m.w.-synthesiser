#pragma once

#include <math.h>
#include "DebugLog.h"

//-------------------------------------------------------------------
enum envelope_state
	{
		ENV_IDLE,
		ENV_ATTACK,
		ENV_DECAY,
		ENV_SUSTAIN,
		ENV_RELEASE
	};

//-------------------------------------------------------------------
class CEnvelope
{
public:
	CEnvelope();
	~CEnvelope(void);

	void  setAttackTime	  (float secs);
	void  setDecayTime	  (float secs);
	void  setSustainLevel (float level);
	void  setReleaseTime  (float secs);
	void  setSampleRate   (float sr);

	void  setGate  (bool isOn);

	int   getState ()  { return state; }
	void  reset	   ();

	float process  ();
	float getOutput()  { return output; }

	bool  isNumber (double x);

private:
	float attTime;
	float decTime;
	float susLevel;
	float relTime;

	float attCoeff;
	float decCoeff;
	float relCoeff;

	float sampleRate;
	float output;

	unsigned int state;

	float updateCoeff	  (float secs, double begin, double end);
};

// functions kept inline for efficiency

inline void CEnvelope::setGate (bool isOn)
{
	if (isOn) state = ENV_ATTACK;
	else if (state != ENV_IDLE) state = ENV_RELEASE;
}

inline float CEnvelope::process()
{
	switch (state)
	{
		case ENV_ATTACK:
			{
				if (output <= 0.f)
					output = 0.0001f; // start here
				
				output *= attCoeff;
				
				if (output >= 1.f)
				{
					output = 1.f;
					state  = ENV_DECAY;
				}
			}break;
		case ENV_DECAY:
			{
				output *= decCoeff;
				if (output <= susLevel)
				{
					output = susLevel;
					state  = ENV_SUSTAIN;
				}
			}break;
		case ENV_SUSTAIN: output = susLevel; break;
		case ENV_RELEASE:
			{
				output *= relCoeff;
				if (output <= 0.001f) // if less than -60 dB
				{
					output = 0.f;		// set the output to zero
					state  = ENV_IDLE;	// indicate that the envelope is idle
				}
			}break;
		case ENV_IDLE: output = 0.0; break;
	}
	return output;
}