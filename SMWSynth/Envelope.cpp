#include "Envelope.h"
#include "TabOsc.h"

//-------------------------------------------------------------------
CEnvelope::CEnvelope()
{
	attTime  = 0.1f; // times in secs
	decTime  = 0.5f;
	susLevel = 1.0f;  // 0.1 = full gain
	relTime  = 1.0f;

	sampleRate = 44100.0;

	setAttackTime(attTime);
	setSustainLevel(susLevel);
	setDecayTime(decTime);
	setReleaseTime(relTime);

	reset(); // zero output, state to idle
}

//-------------------------------------------------------------------
CEnvelope::~CEnvelope(void)
{
	
}

void CEnvelope::reset()
{ 
	setGate(false); 
	output = 0.f; // -60dB 
}

//-------------------------------------------------------------------
void CEnvelope::setAttackTime(float secs)
{
	attTime  = secs;
	attCoeff = updateCoeff (attTime, 0.0001, 1.001);
}

//-------------------------------------------------------------------
void CEnvelope::setDecayTime(float secs)
{
	decTime  = secs;
	decCoeff = updateCoeff (decTime, 1.0, susLevel - 0.001);
}

//-------------------------------------------------------------------
void CEnvelope::setSustainLevel(float level)
{
	susLevel = level;
}

//-------------------------------------------------------------------
void CEnvelope::setReleaseTime(float secs)
{
	relTime  = secs;
	relCoeff = updateCoeff (relTime, susLevel, 0.0001);
}

void CEnvelope::setSampleRate(float sr)
{
	sampleRate = sr;
}

//-------------------------------------------------------------------
float CEnvelope::updateCoeff(float secs, double begin, double end)
{
	float out = 0.f;
	out = 1.f + (float)(log(end) - log(begin)) / (secs * sampleRate);
	
	return out;
}

// this checks numbers to see if they are valid
bool CEnvelope::isNumber(double x) 
{
    // This looks like it should always be true, 
    // but it's false if x is a NaN.
    return (x == x); 
}