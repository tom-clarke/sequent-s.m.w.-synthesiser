#pragma once

#include "vstgui.h"

class CSmwButton : public CKickButton
{
public:
	CSmwButton(const CRect& size, CControlListener* listener, long tag, CBitmap* background, const CPoint& offset);
	~CSmwButton(void);

	CMouseEventResult onMouseDown  (CPoint& where, const long& buttons);
	CMouseEventResult onMouseUp    (CPoint& where, const long& buttons);
	CMouseEventResult onMouseMoved (CPoint& where, const long& buttons);

protected:
	CPoint offset;

private:
	float fEntryState;

};

