# README #

Sequent: Sequential Multiple Wavetable Synthesiser

### What is this repository for? ###

This repository contains a VST audio plugin project in Visual Studio 2010.

* Quick summary
* Version

### How do I get set up? ###

You will need the VST 2.4 SDK (which isn't available from Steinberg anymore so another download source must be found) and the [VSTGUI 3.6 SDK](http://sourceforge.net/projects/vstgui/) for the projects dependent files.

A good guide to get set-up for VST 2.4 development can be found at [Terragon Audio](http://teragonaudio.com/article/How-to-make-VST-plugins-in-Visual-Studio.html)'s site.

### Contribution guidelines ###


### Who do I talk to? ###

Repo owner: Tom Clarke

Email: tomclarke4@hotmail.com